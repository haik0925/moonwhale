#define bada_vk_exported_function(fn) PFN_##fn fn;
#define bada_vk_global_level_function(fn) PFN_##fn fn;
#define bada_vk_instance_level_function(fn) PFN_##fn fn;
#define bada_vk_device_level_function(fn) PFN_##fn fn;
#include "bada_vk_func_def.inl"

#ifdef _WIN32
inline void* bada__load_proc_address(void* library, const char* proc) { return GetProcAddress((HMODULE)library, proc); }
#endif//_WIN32

bool bada__is_available_vk_extension(const char* required_ext_name, VkExtensionProperties* available_exts, bada_u32 available_ext_count);
bool bada__init_swapchain(BadaSwapchain* swapchain, VkPhysicalDevice physical_device, VkSurfaceKHR surface, VkDevice device, VkSwapchainKHR old);
VkRenderPass bada__create_render_pass(VkDevice device, BadaSwapchain* swapchain);
bool bada__init_pipeline(BadaPipeline* pipeline, VkDevice device, VkRenderPass render_pass, VkDescriptorSetLayout desc_set_layout);
VkImageView bada__create_image_view(VkDevice device, VkImage image, VkFormat view_format, bada_u32 base_mip_level, bada_u32 level_count, bada_u32 base_array_layer, bada_u32 layer_count);
bool bada__init_image(BadaImage* image, VkDevice device, bada_u32 width, bada_u32 height);
VkDeviceMemory bada__allocate_device_memory(VkPhysicalDevice physical_device, VkDevice device, VkMemoryRequirements requirements, VkMemoryPropertyFlags property);

inline
void bada__test_copy_uniform_buffer(BadaBuffer* uniform_buffer, VkPhysicalDevice physical_device, VkDevice device, BadaSwapchain* swapchain, BadaBuffer* staging_buffer, void* staging_buffer_memptr, VkCommandBuffer cmd_buffer, VkQueue queue)
{
    vkDeviceWaitIdle(device);

    //float half_view_height = 0.7f;
    //float half_view_width = half_view_height * (float)swapchain->extent.width / (float)swapchain->extent.height;
    //auto projection = Mat4::ortho(-half_view_width, half_view_width, -half_view_height, half_view_height, -1.f, 1.f);
    float projection[16] = {
        1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1,
    };
    bada_stage_buffer_data(staging_buffer, staging_buffer_memptr, physical_device, device, projection, sizeof(projection));

    VkCommandBufferBeginInfo cmd_buffer_bi = { VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO };
    cmd_buffer_bi.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    vkBeginCommandBuffer(cmd_buffer, &cmd_buffer_bi);
    VkBufferCopy buffer_copy = {};
    buffer_copy.size = uniform_buffer->size;
    vkCmdCopyBuffer(cmd_buffer, staging_buffer->handle, uniform_buffer->handle, 1, &buffer_copy);
    VkBufferMemoryBarrier buffer_memory_barrier = { VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER };
    buffer_memory_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    buffer_memory_barrier.dstAccessMask = VK_ACCESS_UNIFORM_READ_BIT;
    buffer_memory_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    buffer_memory_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    buffer_memory_barrier.buffer = uniform_buffer->handle;
    buffer_memory_barrier.size = VK_WHOLE_SIZE;
    vkCmdPipelineBarrier(cmd_buffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_VERTEX_SHADER_BIT,
        0, 0, nullptr, 1, &buffer_memory_barrier, 0, nullptr);
    vkEndCommandBuffer(cmd_buffer);

    VkSubmitInfo submit_info = { VK_STRUCTURE_TYPE_SUBMIT_INFO };
    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &cmd_buffer;
    bada_vk_check(vkQueueSubmit(queue, 1, &submit_info, VK_NULL_HANDLE));
    vkDeviceWaitIdle(device);
}

#if bada_debug
VKAPI_ATTR
VkBool32 VKAPI_CALL bada__vk_debug_callback(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT obj_type, bada_u64 obj, size_t location, bada_i32 code, const char* layer_prefix, const char* msg, void* userdata);
#endif