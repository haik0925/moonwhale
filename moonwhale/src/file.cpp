#ifdef _WIN32
#include "file.h"
#include "win32.h"
#include "logger.h"

FileReadResult read_file(const char* filename)
{
    HANDLE file_handle =
        CreateFileA(filename, GENERIC_READ, FILE_SHARE_READ, nullptr, OPEN_EXISTING, 0, 0);
    if (file_handle == INVALID_HANDLE_VALUE)
    {
        l_error("Failed to open %s", filename);
        return {};
    }

    scope_exit{ CloseHandle(file_handle); };

    LARGE_INTEGER file_size;
    if (!GetFileSizeEx(file_handle, &file_size))
    {
        l_error("Failed to get file size of %s", filename);
        return {};
    }

    void* contents = malloc(file_size.QuadPart);
    if (!contents)
    {
        l_error("Failed to allocate memory");
        return {};
    }

    uint file_size32 = static_cast<uint>(file_size.QuadPart);
    DWORD bytes_read;
    if (!ReadFile(file_handle, contents, file_size32, &bytes_read, nullptr) ||
        file_size32 != bytes_read)
    {
        l_error("Bytes read is not same as file size of %s", filename);
        free(contents);
        return {};
    }

    return { true, contents, file_size32 };
}

bool write_file(const char* filename, void* contents, uint size)
{
    HANDLE file_handle =
        CreateFileA(filename, GENERIC_WRITE, 0, nullptr, CREATE_ALWAYS, 0, 0);

    if (file_handle == INVALID_HANDLE_VALUE)
    {
        l_error("Failed to open %s", filename);
        return false;
    }

    DWORD bytes_written;
    bool result = false;
    if (WriteFile(file_handle, contents, size, &bytes_written, nullptr))
    {
        result = (bytes_written == size);
    }
    CloseHandle(file_handle);

    return result;
}

#endif//_WIN32