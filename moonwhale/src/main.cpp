#include "win32.h"
#include "input.h"
#include "bada.h"
#include "vulkan_examples.h"

#define VULKAN_EXAMPLE_TYPE RenderTriangle

int main()
{
    win32::initAll();

    bool running = true;

    win32::Window window(1280, 720, "game_lab");

    BadaContext bada;

    BadaInitParams params = { GetModuleHandle(nullptr), window.m_window };
    if (!bada_init(&bada, &params))
    {
        printf("Failed to init bada");
        return 1;
    }

    VulkanExample vulkan_example = {};
    vulkan_example_init(&vulkan_example, VulkanExampleType::VULKAN_EXAMPLE_TYPE, &bada);

    struct UserData { VulkanExample* example; BadaContext* bada; bool* running; }
    userdata = { &vulkan_example, &bada, &running };

    window.m_userData = &userdata;
    window.m_onClose = [](void* userData)
    {
        *((UserData*)userData)->running = false;
    };
    window.m_onResize = [](void* userdata, unsigned width, unsigned height)
    {
        bada_on_window_size_changed(((UserData*)userdata)->bada);
        vulkan_example_on_window_size_changed(((UserData*)userdata)->example);
    };

    input::Input input = {};
    auto targetDt = 1.f / 60.f;

    auto lastCounter = win32::getWallClockCounter();

    while (running)
    {
        for (int i = 0; i < common::arrayCount(input.m_buttons); ++i)
        {
            auto button = &input.m_buttons[i];
            button->m_isPressed = false;
            button->m_isReleased = false;
        }

        if (!win32::handleLoopMessage(&window, &input))
        {
            break;
        }

        input.m_dt = targetDt;
        POINT mousePoint;
        GetCursorPos(&mousePoint);
        ScreenToClient(window.m_window, &mousePoint);
        input.m_mouse.x = mousePoint.x;
        input.m_mouse.y = mousePoint.y;
        input.m_windowSize = win32::getWindowSize(window.m_window);

        vulkan_example_update(&vulkan_example);

        auto currentCounter = win32::getWallClockCounter();
        float dt = win32::getElapsedTime(lastCounter, currentCounter);
        while (dt < targetDt)
        {
            currentCounter = win32::getWallClockCounter();
            dt = win32::getElapsedTime(lastCounter, currentCounter);
        }
        lastCounter = currentCounter;
    }

    vulkan_example_deinit(&vulkan_example);
    bada_deinit(&bada);

    return 0;
}