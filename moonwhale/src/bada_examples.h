#ifndef BADA_EXAPMLES_H
#define BADA_EXAPMLES_H

#include "bada.h"

inline
VkDescriptorPool bada_ex_create_descriptor_pool(VkDevice device, VkDescriptorPoolSize* pool_sizes, bada_u32 pool_size_count, bada_u32 max_set_count)
{
    VkDescriptorPool descriptor_pool;
    VkDescriptorPoolCreateInfo create_info = { VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO };
    create_info.maxSets = max_set_count;
    create_info.poolSizeCount = pool_size_count;
    create_info.pPoolSizes = pool_sizes;
    bada_vk_check(vkCreateDescriptorPool(device, &create_info, nullptr, &descriptor_pool));
    return descriptor_pool;
}

inline
void bada_ex_init_descriptor_set(BadaDescriptorSet* descriptor_set, VkDevice device, VkDescriptorSetLayoutBinding* bindings, bada_u32 binding_count, VkDescriptorPool pool)
{
    VkDescriptorSetLayout layout;
    {
        VkDescriptorSetLayoutCreateInfo create_info = { VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO };
        create_info.bindingCount = binding_count;
        create_info.pBindings = bindings;
        bada_vk_check(vkCreateDescriptorSetLayout(device, &create_info, nullptr, &layout));
    }

    VkDescriptorSet handle;
    {
        VkDescriptorSetAllocateInfo allocate_info = { VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO };
        allocate_info.descriptorPool = pool;
        allocate_info.descriptorSetCount = 1;
        allocate_info.pSetLayouts = &layout;
        bada_vk_check(vkAllocateDescriptorSets(device, &allocate_info, &handle));
    }

    *descriptor_set = {};
    descriptor_set->layout = layout;
    descriptor_set->handle = handle;
}

enum class BadaExDescriptorType
{
    Image,
    UniformBuffer,
};

struct BadaExDescriptorInfo
{
    BadaExDescriptorType type;
    BadaImage* image;
    BadaBuffer* uniform_buffer;
};

inline
void bada_ex_write_descriptor(BadaDescriptorSet* descriptor_set, VkDevice device, BadaExDescriptorInfo* descriptor_infos, bada_u32 descriptor_info_count)
{
    bada_vla(writes, VkWriteDescriptorSet, descriptor_info_count);
    bada_vla(image_infos, VkDescriptorImageInfo, descriptor_info_count);
    bada_u32 last_used_image_info_index = 0;
    bada_vla(buffer_infos, VkDescriptorBufferInfo, descriptor_info_count);
    bada_u32 last_used_buffer_info_index = 0;

    for (bada_u32 i = 0; i < writes.count; ++i)
    {
        auto write = &writes.data[i];
        auto descriptor_info = &descriptor_infos[i];

        *write = { VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET };
        write->dstSet = descriptor_set->handle;
        write->dstBinding = i;
        write->descriptorCount = 1;

        switch (descriptor_info->type)
        {
        case BadaExDescriptorType::Image:
        {
            auto image_info = &image_infos[last_used_image_info_index++];
            *image_info = {};
            image_info->sampler = descriptor_info->image->sampler;
            image_info->imageView = descriptor_info->image->view;
            image_info->imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

            write->descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
            write->pImageInfo = image_info;
        } break;
        case BadaExDescriptorType::UniformBuffer:
        {
            auto buffer_info = &buffer_infos[last_used_buffer_info_index++];
            *buffer_info = {};
            buffer_info->buffer = descriptor_info->uniform_buffer->handle;
            buffer_info->range = descriptor_info->uniform_buffer->size;
        }
        default:
            bada_assert(false);
            break;
        }
    }

    vkUpdateDescriptorSets(device, writes.count, writes.data, 0, nullptr);
}

#endif//BADA_EXAPMLES_H
