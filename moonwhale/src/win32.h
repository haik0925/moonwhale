#pragma once

#ifndef _WIN32
#error The platform is not Windows
#endif//_WIN32

#include <Windows.h>
#include "common.h"
#include "logger.h"
#include "math.h"

namespace input
{
struct Input;
}

namespace win32
{

bool initAll();

#define on_window_close_callback(name) void name(void*)
typedef on_window_close_callback(OnWindowCloseCallback);
inline on_window_close_callback(OnWindowCloseCallbackStub) {}

#define on_window_resize_callback(name) void name(void* userdata, unsigned width, unsigned height)
typedef on_window_resize_callback(OnWindowResizeCallback);
inline on_window_resize_callback(on_window_resize_callback_stub) {}

struct Window
{
    HWND m_window = nullptr;
    HDC m_dc = nullptr;
    int m_width = 0;
    int m_height = 0;
    bool m_movingOrResizing = false;
    bool m_resizing = false;

    OnWindowCloseCallback* m_onClose = &OnWindowCloseCallbackStub;
    OnWindowResizeCallback* m_onResize = &on_window_resize_callback_stub;
    void* m_userData = nullptr;

    Window(int width, int height, const char* windowName);
    ~Window();
};

Vec2i getWindowSize(HWND window);
bool handleLoopMessage(Window* window, input::Input* input);
LARGE_INTEGER getWallClockCounter();
float getElapsedTime(LARGE_INTEGER start, LARGE_INTEGER end);

LRESULT CALLBACK mainWindowCallback(HWND window, UINT message, WPARAM wp, LPARAM lp);

}//win32