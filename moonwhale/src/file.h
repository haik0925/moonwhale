#pragma once
#include "common.h"
#include <cstdlib>

struct FileReadResult
{
    bool success;
    void* contents;
    size_t size;
};

FileReadResult read_file(const char* filename);
bool write_file(const char* filename, void* contents, uint size);