#ifndef BADA_H
#define BADA_H
#ifdef _WIN32
#define VK_USE_PLATFORM_WIN32_KHR
#else
#error "Not supported platform"
#endif//_WIN32
#define VK_NO_PROTOTYPES
#include <vulkan/vulkan.h>
#include <stdint.h>

#ifdef _WIN32
#include <malloc.h>//alloca
#endif//_WIN32

#ifndef bada_printf
#include <stdio.h>
#define bada_printf(format, ...) printf(format, __VA_ARGS__)
#endif//bada_log_error

#ifndef bada_assert
#include <assert.h>
#define bada_assert(expression) assert(expression)
#endif//bada_assert

#ifndef bada_log_error
#define bada_log_error(format, ...) bada_printf("[%s:%d] " format "\n", __FILE__, __LINE__, __VA_ARGS__)
#endif//bada_log_error

#ifndef bada_log_trace
#define bada_log_trace(format, ...) bada_printf("[%s:%d] " format "\n", __FILE__, __LINE__, __VA_ARGS__)
#endif//bada_log_trace

#define bada_debug 1
#define bada_swapchain_image_count 2

#define bada_vk_check(expression) bada_assert(expression == VK_SUCCESS);

typedef int32_t bada_i32;
typedef uint8_t bada_u8;
typedef uint32_t bada_u32;
typedef uint64_t bada_u64;

template <typename T>
struct BadaValArray
{
    T* data;
    bada_u32 count;

    T& operator[](bada_u32 index)
    {
        bada_assert(index < count);
        return data[index];
    }
};

struct BadaScopeExitDummy
{
    int reserved;
};
template <typename F>
struct BadaScopeExit
{
    F m_f;
    BadaScopeExit(F f) : m_f(f) {}
    ~BadaScopeExit() { m_f(); }
};
template <typename F>
inline BadaScopeExit<F> operator+(BadaScopeExitDummy dummy, F f) { return BadaScopeExit<F>(f); }
#define bada_make_scope_exit_name(prefix, line) bada_make_scope_exit_name2(prefix, line)
#define bada_make_scope_exit_name2(prefix, line) prefix ## line
#define bada_scope_exit \
    auto bada_make_scope_exit_name(temp___scopeExit, __LINE__) = BadaScopeExitDummy() + [&]()

// Variable-length array (created via alloca)
//#define bada_vla(name, type, count_) \
//struct { type* data; bada_u32 count; } name = \

#define bada_vla(name, type, count_) \
BadaValArray<type> name = \
{(type*)alloca((count_) * sizeof(type)), (count_)}
#define bada_alloca(type, count) (type*)alloca(count * sizeof(type))
template <typename TArr, bada_u32 N>
inline constexpr
bada_u32 bada_array_count(TArr(&)[N]) { return N; }
template <typename TArr, bada_u32 N>
inline
void bada_array_copy(TArr(&from)[N], TArr(&to)[N])
{
    for (bada_u32 i = 0; i < N; ++i)
    {
        to[i] = from[i];
    }
}


struct BadaSwapchain
{
    VkSwapchainKHR handle;
    VkExtent2D extent;
    VkFormat format;
    VkImage images[bada_swapchain_image_count];
};

struct BadaFramebuffer
{
    VkFramebuffer handle;
    VkImageView image_view;
};

struct BadaVertex
{
    float x, y, z, w;
    float r, g, b, a;
    float u, v;
};

struct BadaBuffer
{
    bada_u32 size;
    VkBuffer handle;
    VkDeviceMemory memory;
};

struct BadaRenderingResource
{
    VkSemaphore image_available_semaphore;
    VkSemaphore rendering_finished_semaphore;
    VkFence fence;
    BadaFramebuffer framebuffer;
    VkCommandBuffer cmd_buffer;
};

struct BadaImage
{
    bada_u32 width, height;
    VkImage handle;
    VkDeviceMemory memory;
    VkImageView view;
    VkSampler sampler;
};

struct BadaDescriptorSet
{
    VkDescriptorSetLayout layout;
    VkDescriptorSet handle;
};

struct BadaPipeline
{
    VkPipeline handle;
    VkPipelineLayout layout;
};

struct BadaRenderContext
{
    bada_u32 image_index;
    BadaRenderingResource* rendering_resource;
};

struct BadaContext
{
    void* library;
    VkInstance instance;
#if bada_debug
    VkDebugReportCallbackEXT debug_report_callback;
#else
    bada_u8 padding[sizeof(VkDebugReportCallbackEXT)];
#endif
    VkSurfaceKHR surface;
    VkPhysicalDevice physical_device;
    bada_u32 queue_family_index;
    VkDevice device;
    VkQueue queue;
    VkCommandPool cmd_pool;
    BadaSwapchain swapchain;
    //VkRenderPass render_pass;
    //BadaPipeline pipeline;
    BadaRenderingResource rendering_resources[2];
    bada_u32 next_resource_index;
    //BadaBuffer vertex_buffer;
    //BadaBuffer staging_buffer;
    //void* staging_buffer_memptr;

    //BadaImage test_image;
    //VkDescriptorPool test_desc_pool;
    //BadaDescriptorSet test_desc_set;

    //BadaBuffer test_uniform_buffer;
};

struct BadaInitParams
{
    HINSTANCE exeInstance;
    HWND window;
};

bool bada_init(BadaContext* bada, const BadaInitParams* params);
void bada_deinit(BadaContext* bada);
bool bada_on_window_size_changed(BadaContext* bada);
bool bada_render_begin(BadaContext* bada, BadaRenderContext* render_context, VkRenderPass render_pass);
bool bada_render_end(BadaContext* bada, BadaRenderContext* render_context);
[[deprecated]]
bool bada_render(BadaContext* bada);

VkShaderModule bada_create_shader_module(VkDevice device, const char* filename);
bool bada_init_buffer(BadaBuffer* buffer, VkPhysicalDevice physical_device, VkDevice device, bada_u32 size, VkBufferUsageFlags usage, VkMemoryPropertyFlags memory_property);
void bada_deinit_buffer(BadaBuffer* buffer, VkDevice device);
void bada_stage_buffer_data(BadaBuffer* staging_buffer, void* staging_buffer_memptr, VkPhysicalDevice physical_device, VkDevice device, void* data, bada_u32 size);
void bada_deinit_pipeline(BadaPipeline* pipeline, VkDevice device);

#define bada_vk_exported_function(fn) extern PFN_##fn fn;
#define bada_vk_global_level_function(fn) extern PFN_##fn fn;
#define bada_vk_instance_level_function(fn) extern PFN_##fn fn;
#define bada_vk_device_level_function(fn) extern PFN_##fn fn;
#include "bada_vk_func_def.inl"

#endif//BADA_H