#version 450

layout(set = 0, binding = 1) uniform u_uniform_buffer {
    uniform mat4 u_projection;
};

layout(location = 0) in vec4 i_position;
layout(location = 1) in vec4 i_color;
layout(location = 2) in vec2 i_uv;

out gl_PerVertex
{
    vec4 gl_Position;
};

layout(location = 0) out vec4 v_color;
layout(location = 1) out vec2 v_uv;

void main()
{
    gl_Position = u_projection * i_position;
    v_color = i_color;
    v_uv = i_uv;
}