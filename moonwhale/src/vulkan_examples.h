#ifndef VULKAN_EXAMPLES_H
#define VULKAN_EXAMPLES_H

#include "bada.h"
#include "math.h"

enum VulkanExampleType
{
    RenderTriangle,
    RenderQuad,
    IndexBuffer,
    DepthBuffer,
};

struct VulkanExampleRenderTriangle
{
    struct Vertex
    {
        Vec3 pos;
        Vec3 color;
    };

    VulkanExampleType type;
    BadaContext* bada;

    VkRenderPass render_pass;
    VkDescriptorPool descriptor_pool;
    BadaDescriptorSet descriptor_set;
    VkShaderModule vert_shader;
    VkShaderModule frag_shader;
    BadaPipeline pipeline;
    BadaBuffer staging_buffer;
    BadaBuffer vertex_buffer;
    BadaBuffer uniform_buffer;
};

struct VulkanExampleRenderQuad
{
    VulkanExampleType type;
    BadaContext* bada;
};

union VulkanExample
{
    struct
    {
        VulkanExampleType type;
        BadaContext* bada;
    };
    VulkanExampleRenderTriangle render_triangle;
    VulkanExampleRenderQuad render_quad;
};

void vulkan_example_init(VulkanExample* example, VulkanExampleType type, BadaContext* bada);
void vulkan_example_update(VulkanExample* example);
void vulkan_example_deinit(VulkanExample* example);
void vulkan_example_on_window_size_changed(VulkanExample* example);

void vulkan_example_render_triangle_init(VulkanExampleRenderTriangle* example);
void vulkan_example_render_triangle_update_and_render(VulkanExampleRenderTriangle* example);
void vulkan_example_render_triangle_deinit(VulkanExampleRenderTriangle* example);
void vulkan_example_render_triangle_on_window_size_changed(VulkanExampleRenderTriangle* example);

void vulkan_example_render_quad_init(VulkanExampleRenderQuad* example);
void vulkan_example_render_quad_update_and_render(VulkanExampleRenderQuad* example);
void vulkan_example_render_quad_deinit(VulkanExampleRenderQuad* example);
void vulkan_example_render_quad_on_window_size_changed(VulkanExampleRenderQuad* example);

#endif//VULKAN_EXAMPLES_H