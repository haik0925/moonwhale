#include "vulkan_examples.h"

#define vulkan_example_assert(exp) bada_assert(exp)

void vulkan_example_init(VulkanExample* example, VulkanExampleType type, BadaContext* bada)
{
    example->type = type;
    example->bada = bada;
    switch (example->type)
    {
    case VulkanExampleType::RenderTriangle:
        vulkan_example_render_triangle_init(&example->render_triangle);
        break;
    case VulkanExampleType::RenderQuad:
        vulkan_example_render_quad_init(&example->render_quad);
        break;
    default:
        vulkan_example_assert(false);
    }
}

void vulkan_example_update(VulkanExample* example)
{
    switch (example->type)
    {
    case VulkanExampleType::RenderTriangle:
        vulkan_example_render_triangle_update_and_render(&example->render_triangle);
        break;
    case VulkanExampleType::RenderQuad:
        vulkan_example_render_quad_update_and_render(&example->render_quad);
        break;
    default:
        vulkan_example_assert(false);
    }
}

void vulkan_example_deinit(VulkanExample* example)
{
    switch (example->type)
    {
    case VulkanExampleType::RenderTriangle:
        vulkan_example_render_triangle_deinit(&example->render_triangle);
        break;
    case VulkanExampleType::RenderQuad:
        vulkan_example_render_quad_deinit(&example->render_quad);
        break;
    default:
        vulkan_example_assert(false);
    }
}

void vulkan_example_on_window_size_changed(VulkanExample* example)
{
    switch (example->type)
    {
    case VulkanExampleType::RenderTriangle:
        vulkan_example_render_triangle_on_window_size_changed(&example->render_triangle);
        break;
    case VulkanExampleType::RenderQuad:
        vulkan_example_render_quad_on_window_size_changed(&example->render_quad);
        break;
    default:
        vulkan_example_assert(false);
    }
}

void vulkan_example_render_triangle__reset_pipeline(VulkanExampleRenderTriangle* example)
{
    auto device = example->bada->device;
    bada_deinit_pipeline(&example->pipeline, device);

    vulkan_example_assert(example->vert_shader && example->frag_shader);

    VkPipelineShaderStageCreateInfo vert_shader_stage_ci = { VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO };
    vert_shader_stage_ci.stage = VK_SHADER_STAGE_VERTEX_BIT;
    vert_shader_stage_ci.module = example->vert_shader;
    vert_shader_stage_ci.pName = "main";
    VkPipelineShaderStageCreateInfo frag_shader_stage_ci = { VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO };
    frag_shader_stage_ci.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
    frag_shader_stage_ci.module = example->frag_shader;
    frag_shader_stage_ci.pName = "main";
    VkPipelineShaderStageCreateInfo shader_stage_cis[] =
    {
        vert_shader_stage_ci,
        frag_shader_stage_ci,
    };

    VkVertexInputBindingDescription vert_binding_descs[1] = {};
    vert_binding_descs[0].binding = 0;
    vert_binding_descs[0].stride = sizeof(VulkanExampleRenderTriangle::Vertex);
    vert_binding_descs[0].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

    VkVertexInputAttributeDescription vert_pos_attrib_desc = {};
    vert_pos_attrib_desc.location = 0;
    vert_pos_attrib_desc.binding = vert_binding_descs[0].binding;
    vert_pos_attrib_desc.format = VK_FORMAT_R32G32B32_SFLOAT;
    vert_pos_attrib_desc.offset = offsetof(VulkanExampleRenderTriangle::Vertex, pos);

    VkVertexInputAttributeDescription vert_color_attrib_desc = {};
    vert_color_attrib_desc.location = 1;
    vert_color_attrib_desc.binding = vert_binding_descs[0].binding;
    vert_color_attrib_desc.format = VK_FORMAT_R32G32B32_SFLOAT;
    vert_color_attrib_desc.offset = offsetof(VulkanExampleRenderTriangle::Vertex, color);

    VkVertexInputAttributeDescription vert_attrib_descs[] =
    {
        vert_pos_attrib_desc,
        vert_color_attrib_desc,
    };

    VkPipelineVertexInputStateCreateInfo vert_input_state_ci = { VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO };
    vert_input_state_ci.vertexBindingDescriptionCount = bada_array_count(vert_binding_descs);
    vert_input_state_ci.pVertexBindingDescriptions = vert_binding_descs;
    vert_input_state_ci.vertexAttributeDescriptionCount = bada_array_count(vert_attrib_descs);
    vert_input_state_ci.pVertexAttributeDescriptions = vert_attrib_descs;

    VkPipelineInputAssemblyStateCreateInfo input_assembly_state_ci = { VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO };
    input_assembly_state_ci.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;

    VkViewport viewport = {};
    viewport.width = static_cast<float>(example->bada->swapchain.extent.width);
    viewport.height = static_cast<float>(example->bada->swapchain.extent.height);
    viewport.minDepth = 0.f;
    viewport.maxDepth = 1.f;
    VkRect2D scissor = {};
    scissor.extent = example->bada->swapchain.extent;

    VkPipelineViewportStateCreateInfo viewport_state_ci = { VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO };
    viewport_state_ci.viewportCount = 1;
    viewport_state_ci.pViewports = &viewport;
    viewport_state_ci.scissorCount = 1;
    viewport_state_ci.pScissors = &scissor;

    VkPipelineRasterizationStateCreateInfo rasterization_state_ci = { VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO };
    rasterization_state_ci.polygonMode = VK_POLYGON_MODE_FILL;
    rasterization_state_ci.cullMode = VK_CULL_MODE_BACK_BIT;
    rasterization_state_ci.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    rasterization_state_ci.lineWidth = 1.f;

    VkPipelineMultisampleStateCreateInfo multisample_state_ci = { VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO };
    multisample_state_ci.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    multisample_state_ci.minSampleShading = 1.f;

    VkPipelineColorBlendAttachmentState color_blend_attachment_state = {};
    color_blend_attachment_state.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
    color_blend_attachment_state.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
    color_blend_attachment_state.colorBlendOp = VK_BLEND_OP_ADD;
    color_blend_attachment_state.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
    color_blend_attachment_state.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    color_blend_attachment_state.alphaBlendOp = VK_BLEND_OP_ADD;
    color_blend_attachment_state.colorWriteMask =
        VK_COLOR_COMPONENT_R_BIT |
        VK_COLOR_COMPONENT_G_BIT |
        VK_COLOR_COMPONENT_B_BIT |
        VK_COLOR_COMPONENT_A_BIT;

    VkPipelineColorBlendStateCreateInfo color_blend_state_ci = { VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO };
    color_blend_state_ci.logicOp = VK_LOGIC_OP_COPY;
    color_blend_state_ci.attachmentCount = 1;
    color_blend_state_ci.pAttachments = &color_blend_attachment_state;

    VkPipelineDynamicStateCreateInfo dynamic_state_ci = { VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO };

    VkPipelineLayoutCreateInfo layout_ci = { VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO };
    layout_ci.setLayoutCount = 1;
    layout_ci.pSetLayouts = &example->descriptor_set.layout;
    bada_vk_check(vkCreatePipelineLayout(device, &layout_ci, nullptr, &example->pipeline.layout));

    VkGraphicsPipelineCreateInfo pipeline_ci = { VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO };
    pipeline_ci.stageCount = bada_array_count(shader_stage_cis);
    pipeline_ci.pStages = shader_stage_cis;
    pipeline_ci.pVertexInputState = &vert_input_state_ci;
    pipeline_ci.pInputAssemblyState = &input_assembly_state_ci;
    pipeline_ci.pViewportState = &viewport_state_ci;
    pipeline_ci.pRasterizationState = &rasterization_state_ci;
    pipeline_ci.pMultisampleState = &multisample_state_ci;
    pipeline_ci.pColorBlendState = &color_blend_state_ci;
    pipeline_ci.pDynamicState = &dynamic_state_ci;
    pipeline_ci.layout = example->pipeline.layout;
    pipeline_ci.renderPass = example->render_pass;
    pipeline_ci.basePipelineIndex = -1;

    bada_vk_check(vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, 1, &pipeline_ci, nullptr, &example->pipeline.handle));
}

void vulkan_example_render_triangle__reset_projection(VulkanExampleRenderTriangle* example)
{
    auto physical_device = example->bada->physical_device;
    auto device = example->bada->device;

    vkDeviceWaitIdle(device);

    void* staging_buffer_memptr;
    bada_vk_check(vkMapMemory(device, example->staging_buffer.memory, 0, example->staging_buffer.size, 0, &staging_buffer_memptr));
    bada_scope_exit{ vkUnmapMemory(device, example->staging_buffer.memory); };

    {
        float half_view_height = 2.f;
        float half_view_width = half_view_height *
            (float)example->bada->swapchain.extent.width /
            (float)example->bada->swapchain.extent.height;
        auto projection = Mat4::ortho(-half_view_width, half_view_width, -half_view_height, half_view_height, -1.f, 1.f);
        bada_stage_buffer_data(&example->staging_buffer, staging_buffer_memptr, physical_device, device,
            &projection, sizeof(projection));

        auto cmd_buffer = example->bada->rendering_resources[0].cmd_buffer;
        VkCommandBufferBeginInfo cmd_buffer_bi = { VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO };
        cmd_buffer_bi.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
        vkBeginCommandBuffer(cmd_buffer, &cmd_buffer_bi);
        VkBufferCopy copy_region = {};
        copy_region.size = example->uniform_buffer.size;
        vkCmdCopyBuffer(cmd_buffer, example->staging_buffer.handle,
            example->uniform_buffer.handle, 1, &copy_region);
        VkBufferMemoryBarrier buffer_memory_barrier = { VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER };
        buffer_memory_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        buffer_memory_barrier.dstAccessMask = VK_ACCESS_UNIFORM_READ_BIT;
        buffer_memory_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        buffer_memory_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        buffer_memory_barrier.buffer = example->uniform_buffer.handle;
        buffer_memory_barrier.size = VK_WHOLE_SIZE;
        vkCmdPipelineBarrier(cmd_buffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_VERTEX_SHADER_BIT,
            0, 0, nullptr, 1, &buffer_memory_barrier, 0, nullptr);
        vkEndCommandBuffer(cmd_buffer);

        VkSubmitInfo submit_info = { VK_STRUCTURE_TYPE_SUBMIT_INFO };
        submit_info.commandBufferCount = 1;
        submit_info.pCommandBuffers = &cmd_buffer;
        bada_vk_check(vkQueueSubmit(example->bada->queue, 1, &submit_info, VK_NULL_HANDLE));
    }


    VkDescriptorBufferInfo buffer_info = {};
    buffer_info.buffer = example->uniform_buffer.handle;
    buffer_info.range = example->uniform_buffer.size;

    VkWriteDescriptorSet write = { VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET };
    write.dstSet = example->descriptor_set.handle;
    write.dstBinding = 0;
    write.descriptorCount = 1;
    write.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    write.pBufferInfo = &buffer_info;

    vkUpdateDescriptorSets(device, 1, &write, 0, nullptr);

    vkDeviceWaitIdle(device);
}

void vulkan_example_render_triangle_init(VulkanExampleRenderTriangle* example)
{
    vulkan_example_assert(example->type == VulkanExampleType::RenderTriangle);

    auto physical_device = example->bada->physical_device;
    auto device = example->bada->device;

    // Init render pass
    {
        VkAttachmentDescription attachment_descriptions[1] = {};
        attachment_descriptions[0].format = example->bada->swapchain.format;
        attachment_descriptions[0].samples = VK_SAMPLE_COUNT_1_BIT;
        attachment_descriptions[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        attachment_descriptions[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        attachment_descriptions[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        attachment_descriptions[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachment_descriptions[0].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        attachment_descriptions[0].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

        VkAttachmentReference color_attachment_refs[1] = {};
        color_attachment_refs[0].attachment = 0;
        color_attachment_refs[0].layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        VkSubpassDescription subpass_descriptions[1] = {};
        auto subpass_desc = &subpass_descriptions[0];
        subpass_desc->pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
        subpass_desc->colorAttachmentCount = bada_array_count(color_attachment_refs);
        subpass_desc->pColorAttachments = color_attachment_refs;

        VkSubpassDependency dependencies[2] = {};

        dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
        dependencies[0].dstSubpass = 0;
        dependencies[0].srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
        dependencies[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dependencies[0].srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
        dependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        dependencies[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

        dependencies[1].srcSubpass = 0;
        dependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;
        dependencies[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dependencies[1].dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
        dependencies[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        dependencies[1].dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
        dependencies[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

        VkRenderPassCreateInfo render_pass_ci = { VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO };
        render_pass_ci.attachmentCount = bada_array_count(attachment_descriptions);
        render_pass_ci.pAttachments = attachment_descriptions;
        render_pass_ci.subpassCount = bada_array_count(subpass_descriptions);
        render_pass_ci.pSubpasses = subpass_descriptions;
        render_pass_ci.dependencyCount = bada_array_count(dependencies);
        render_pass_ci.pDependencies = dependencies;

        bada_vk_check(vkCreateRenderPass(device, &render_pass_ci, nullptr, &example->render_pass));
    }

    // Init Descriptor Pool/Set
    {
        VkDescriptorPoolSize pool_size;
        pool_size.descriptorCount = 1;
        pool_size.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;

        VkDescriptorPoolCreateInfo descriptor_pool_ci = { VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO };
        descriptor_pool_ci.maxSets = 1;
        descriptor_pool_ci.poolSizeCount = 1;
        descriptor_pool_ci.pPoolSizes = &pool_size;

        bada_vk_check(vkCreateDescriptorPool(device, &descriptor_pool_ci, nullptr, &example->descriptor_pool));

        VkDescriptorSetLayoutBinding binding = {};
        binding.binding = 0;
        binding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        binding.descriptorCount = 1;
        binding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

        VkDescriptorSetLayoutCreateInfo descriptor_set_layout_ci = { VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO };
        descriptor_set_layout_ci.bindingCount = 1;
        descriptor_set_layout_ci.pBindings = &binding;
        bada_vk_check(vkCreateDescriptorSetLayout(device, &descriptor_set_layout_ci, nullptr, &example->descriptor_set.layout));

        VkDescriptorSetAllocateInfo descriptor_set_ai = { VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO };
        descriptor_set_ai.descriptorPool = example->descriptor_pool;
        descriptor_set_ai.descriptorSetCount = 1;
        descriptor_set_ai.pSetLayouts = &example->descriptor_set.layout;
        bada_vk_check(vkAllocateDescriptorSets(device, &descriptor_set_ai, &example->descriptor_set.handle));
    }

    example->vert_shader = bada_create_shader_module(device, "example_render_triangle_shader.vert.spv");
    example->frag_shader = bada_create_shader_module(device, "example_render_triangle_shader.frag.spv");

    vulkan_example_render_triangle__reset_pipeline(example);

    bada_init_buffer(&example->staging_buffer, physical_device, device, 1000,
        VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);

    VulkanExampleRenderTriangle::Vertex triangle_vertices[] =
    {
        {{-1.f, 1.f, 0.f}, {1.f, 0.f, 0.f}},
        {{1.f, 1.f, 0.f},  {0.f, 0.f, 1.f}},
        {{0.f, -1.f, 0.f},   {0.f, 1.f, 0.f}},
    };
    bada_init_buffer(&example->vertex_buffer, physical_device, device, sizeof(triangle_vertices),
        VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
    {
        void* staging_buffer_memptr;
        bada_vk_check(vkMapMemory(device, example->staging_buffer.memory, 0, example->staging_buffer.size, 0, &staging_buffer_memptr));
        bada_scope_exit{ vkUnmapMemory(device, example->staging_buffer.memory); };

        bada_stage_buffer_data(&example->staging_buffer, staging_buffer_memptr, physical_device, device, triangle_vertices, example->vertex_buffer.size);

        auto cmd_buffer = example->bada->rendering_resources[0].cmd_buffer;
        VkCommandBufferBeginInfo cmd_buffer_bi = { VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO };
        cmd_buffer_bi.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
        vkBeginCommandBuffer(cmd_buffer, &cmd_buffer_bi);
        VkBufferCopy copy_region = {};
        copy_region.size = example->vertex_buffer.size;
        vkCmdCopyBuffer(cmd_buffer, example->staging_buffer.handle, example->vertex_buffer.handle, 1, &copy_region);
        vkEndCommandBuffer(cmd_buffer);

        VkSubmitInfo submit_info = { VK_STRUCTURE_TYPE_SUBMIT_INFO };
        submit_info.commandBufferCount = 1;
        submit_info.pCommandBuffers = &cmd_buffer;
        vkQueueSubmit(example->bada->queue, 1, &submit_info, VK_NULL_HANDLE);
    }

    vkDeviceWaitIdle(device);

    bada_init_buffer(&example->uniform_buffer, physical_device, device, sizeof(Mat4),
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

    vulkan_example_render_triangle__reset_projection(example);
}

void vulkan_example_render_triangle_update_and_render(VulkanExampleRenderTriangle* example)
{
    vulkan_example_assert(example->type == VulkanExampleType::RenderTriangle);

    BadaRenderContext render_context = {};
    bada_render_begin(example->bada, &render_context, example->render_pass);

    VkRenderPassBeginInfo render_pass_bi = { VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO };
    render_pass_bi.renderPass = example->render_pass;
    render_pass_bi.framebuffer = render_context.rendering_resource->framebuffer.handle;
    render_pass_bi.renderArea.offset = { 0, 0 };
    render_pass_bi.renderArea.extent = example->bada->swapchain.extent;
    render_pass_bi.clearValueCount = 1;
    VkClearValue clear_value = { 1.f, 0.8f, 0.4f, 0.f };
    render_pass_bi.pClearValues = &clear_value;

    auto cmd_buffer = render_context.rendering_resource->cmd_buffer;

    VkCommandBufferBeginInfo cmd_buffer_bi = { VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO };
    cmd_buffer_bi.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    vkBeginCommandBuffer(cmd_buffer, &cmd_buffer_bi);
    vkCmdBeginRenderPass(cmd_buffer, &render_pass_bi, VK_SUBPASS_CONTENTS_INLINE);
    vkCmdBindPipeline(cmd_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, example->pipeline.handle);
    vkCmdBindDescriptorSets(cmd_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, example->pipeline.layout, 0, 1,
        &example->descriptor_set.handle, 0, nullptr);
    VkDeviceSize offset = 0;
    vkCmdBindVertexBuffers(cmd_buffer, 0, 1, &example->vertex_buffer.handle, &offset);
    vkCmdDraw(cmd_buffer, 3, 1, 0, 0);
    vkCmdEndRenderPass(cmd_buffer);
    bada_vk_check(vkEndCommandBuffer(cmd_buffer));

    bada_render_end(example->bada, &render_context);
}

void vulkan_example_render_triangle_deinit(VulkanExampleRenderTriangle* example)
{
    vulkan_example_assert(example->type == VulkanExampleType::RenderTriangle);

    auto device = example->bada->device;

    vkDeviceWaitIdle(device);

    bada_deinit_buffer(&example->uniform_buffer, device);
    bada_deinit_buffer(&example->vertex_buffer, device);
    bada_deinit_buffer(&example->staging_buffer, device);
    bada_deinit_pipeline(&example->pipeline, device);
    vkDestroyShaderModule(device, example->frag_shader, nullptr);
    vkDestroyShaderModule(device, example->vert_shader, nullptr);
    vkDestroyDescriptorSetLayout(device, example->descriptor_set.layout, nullptr);
    vkDestroyDescriptorPool(device, example->descriptor_pool, nullptr);
    vkDestroyRenderPass(device, example->render_pass, nullptr);

    *example = {};
}

void vulkan_example_render_triangle_on_window_size_changed(VulkanExampleRenderTriangle* example)
{
    vulkan_example_render_triangle__reset_pipeline(example);
    vulkan_example_render_triangle__reset_projection(example);
}

void vulkan_example_render_quad_init(VulkanExampleRenderQuad* example)
{
    vulkan_example_assert(example->type == VulkanExampleType::RenderQuad);
}

void vulkan_example_render_quad_update_and_render(VulkanExampleRenderQuad* example)
{
    vulkan_example_assert(example->type == VulkanExampleType::RenderQuad);
}

void vulkan_example_render_quad_deinit(VulkanExampleRenderQuad* example)
{
    vulkan_example_assert(example->type == VulkanExampleType::RenderQuad);
}

void vulkan_example_render_quad_on_window_size_changed(VulkanExampleRenderQuad* example)
{
}
