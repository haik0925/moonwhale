#include "bada.h"
#include "file.h"
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include "bada_internal.h"

bool bada_init(BadaContext* bada, const BadaInitParams* params)
{
#ifdef _WIN32
    auto vulkan_library = (void*)LoadLibraryA("vulkan-1.dll");
#else
    return false;
#endif
    if (!vulkan_library)
    {
        return false;
    }

#define bada_vk_exported_function(fn) \
fn = (PFN_##fn)bada__load_proc_address(vulkan_library, #fn); \
if (!fn) \
{ \
    bada_log_error("Failed to load %s", #fn); \
    return false; \
}
#include "bada_vk_func_def.inl"

#define bada_vk_global_level_function(fn) \
fn = (PFN_##fn)vkGetInstanceProcAddr(nullptr, #fn); \
if (!fn) \
{ \
    bada_log_error("Failed to load %s", #fn); \
    return false; \
}
#include "bada_vk_func_def.inl"


    // Create instance with instance extensions and layers
    VkInstance instance;
    {
        VkApplicationInfo appinfo = { VK_STRUCTURE_TYPE_APPLICATION_INFO };
        appinfo.pApplicationName = "Bada Application Name";
        appinfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
        appinfo.pEngineName = "Bada Engine Name";
        appinfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
        appinfo.apiVersion = VK_API_VERSION_1_0;

        const char* required_exts[] =
        {
            VK_KHR_SURFACE_EXTENSION_NAME,
#if defined(VK_USE_PLATFORM_WIN32_KHR)
            VK_KHR_WIN32_SURFACE_EXTENSION_NAME,
#elif defined(VK_USE_PLATFORM_XCB_KHR)
            VK_KHR_XCB_SURFACE_EXTENSION_NAME,
#elif defined(VK_USE_PLATFORM_XLIB_KHR)
            VK_KHR_XLIB_SURFACE_EXTENSION_NAME,
#endif
#if bada_debug
            VK_EXT_DEBUG_REPORT_EXTENSION_NAME,
#endif
        };
        {
            bada_u32 count = 0;
            bada_vk_check(vkEnumerateInstanceExtensionProperties(nullptr, &count, nullptr));
            bada_vla(available_exts, VkExtensionProperties, count);
            bada_vk_check(vkEnumerateInstanceExtensionProperties(nullptr, &available_exts.count, available_exts.data));

            for (auto required_ext : required_exts)
            {
                if (!bada__is_available_vk_extension(required_ext, available_exts.data, available_exts.count))
                {
                    bada_log_error("Extension %s is not available!", required_ext);
                    return false;
                }
            }
        }

#if bada_debug
        const char* required_layers[] = {
            "VK_LAYER_LUNARG_core_validation",
            "VK_LAYER_LUNARG_object_tracker",
            //"VK_LAYER_LUNARG_pbject_tracker"
        };
        {
            bada_u32 count = 0;
            vkEnumerateInstanceLayerProperties(&count, nullptr);
            bada_vla(available_layers, VkLayerProperties, count);
            vkEnumerateInstanceLayerProperties(&available_layers.count, available_layers.data);
            for (auto required_layer : required_layers)
            {
                for (bada_u32 i = 0; i < available_layers.count; ++i)
                {
                    if (strcmp(required_layer, available_layers.data[i].layerName) == 0)
                    {
                        goto LAYER_IS_AVAILABLE;
                    }
                }
                bada_log_error("Layer %s is not available!", required_layer);
                return false;
                LAYER_IS_AVAILABLE:
                continue;
            }
        }
#endif

        VkInstanceCreateInfo instance_ci = { VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO };
        instance_ci.pApplicationInfo = &appinfo;
        instance_ci.enabledExtensionCount = bada_array_count(required_exts);
        instance_ci.ppEnabledExtensionNames = required_exts;
#if bada_debug
        instance_ci.enabledLayerCount = bada_array_count(required_layers);
        instance_ci.ppEnabledLayerNames = required_layers;
#endif

        bada_vk_check(vkCreateInstance(&instance_ci, nullptr, &instance));
    }

#define bada_vk_instance_level_function(fn) \
fn = (PFN_##fn)vkGetInstanceProcAddr(instance, #fn); \
if (!fn) \
{ \
    bada_log_error("Failed to load %s", #fn); \
    return false; \
}
#include "bada_vk_func_def.inl"

#if bada_debug
    VkDebugReportCallbackEXT debug_report_callback;
    {
        VkDebugReportCallbackCreateInfoEXT report_callback_ci = { VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT };
        report_callback_ci.flags =
            //VK_DEBUG_REPORT_DEBUG_BIT_EXT |
            //VK_DEBUG_REPORT_INFORMATION_BIT_EXT |
            VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT |
            VK_DEBUG_REPORT_WARNING_BIT_EXT |
            VK_DEBUG_REPORT_ERROR_BIT_EXT;
        report_callback_ci.pfnCallback = &bada__vk_debug_callback;
        vkCreateDebugReportCallbackEXT(instance, &report_callback_ci, nullptr, &debug_report_callback);
    }
#endif


    // Create surface
    VkSurfaceKHR surface;
#ifdef _WIN32
    {
        VkWin32SurfaceCreateInfoKHR surface_ci = { VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR };
        surface_ci.hinstance = params->exeInstance;
        surface_ci.hwnd = params->window;

        bada_vk_check(vkCreateWin32SurfaceKHR(instance, &surface_ci, nullptr, &surface));
    }
#else
    return false;
#endif


    // Select physical device
    VkPhysicalDevice physical_device;
    bada_u32 queue_family_index;
    {
        const bada_u32 required_count = 1;
        bada_u32 retrieved_count = required_count;
        VkPhysicalDevice devices[required_count];
        bada_vk_check(vkEnumeratePhysicalDevices(instance, &retrieved_count, devices));
        if (retrieved_count == 0)
        {
            bada_log_error("No physical device!");
            return false;
        }

        for (bada_u32 i = 0; i < retrieved_count; ++i)
        {
            auto device = devices[i];

            bada_u32 count;
            vkGetPhysicalDeviceQueueFamilyProperties(device, &count, nullptr);
            bada_vla(queue_families, VkQueueFamilyProperties, count);
            vkGetPhysicalDeviceQueueFamilyProperties(
                device,
                &queue_families.count, queue_families.data);
            for (bada_u32 j = 0; j < queue_families.count; ++j)
            {
                VkBool32 is_surface_supported;
                vkGetPhysicalDeviceSurfaceSupportKHR(device, j, surface, &is_surface_supported);
                auto properties = queue_families.data[j];
                if ((properties.queueCount > 0) &&
                    (properties.queueFlags & VK_QUEUE_GRAPHICS_BIT) &&
                    is_surface_supported == VK_TRUE)
                {
                    physical_device = device;
                    queue_family_index = j;
                    goto PHYSICAL_DEVICE_AND_QUEUE_FOUND;
                }
            }
        }
        bada_log_error("Valid physical device not found");
        return false;

    }
    PHYSICAL_DEVICE_AND_QUEUE_FOUND:


    // Create logical device and queues
    VkDevice device;
    {
        float queue_priorities[1] = { 1.f };

        VkDeviceQueueCreateInfo queue_ci = { VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO };
        queue_ci.queueFamilyIndex = queue_family_index;
        queue_ci.queueCount = bada_array_count(queue_priorities);
        queue_ci.pQueuePriorities = queue_priorities;

        // Check device extension availability
        const char* required_exts[] =
        {
            VK_KHR_SWAPCHAIN_EXTENSION_NAME
        };
        {
            bada_u32 count;
            bada_vk_check(vkEnumerateDeviceExtensionProperties(physical_device, nullptr, &count, nullptr));
            bada_vla(available_exts, VkExtensionProperties, count);
            bada_vk_check(vkEnumerateDeviceExtensionProperties(physical_device, nullptr, &available_exts.count, available_exts.data));

            for (auto required_ext : required_exts)
            {
                if (!bada__is_available_vk_extension(required_ext, available_exts.data, available_exts.count))
                {
                    bada_log_error("Device extension %s is not available!", required_ext);
                    return false;
                }
            }
        }

        VkDeviceCreateInfo device_ci = { VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO };
        device_ci.queueCreateInfoCount = 1;
        device_ci.pQueueCreateInfos = &queue_ci;
        device_ci.enabledExtensionCount = bada_array_count(required_exts);
        device_ci.ppEnabledExtensionNames = required_exts;

        bada_vk_check(vkCreateDevice(physical_device, &device_ci, nullptr, &device));
    }

#define bada_vk_device_level_function(fn) \
fn = (PFN_##fn)vkGetDeviceProcAddr(device, #fn); \
if (!fn) \
{ \
    bada_log_error("Failed to load %s", #fn); \
    return false; \
}
#include "bada_vk_func_def.inl"

    VkQueue queue;
    vkGetDeviceQueue(device, queue_family_index, 0, &queue);


    // Create command pool
    VkCommandPool cmd_pool;
    {
        VkCommandPoolCreateInfo cmd_pool_ci = { VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO };
        cmd_pool_ci.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT | VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;
        cmd_pool_ci.queueFamilyIndex = queue_family_index;
        bada_vk_check(vkCreateCommandPool(device, &cmd_pool_ci, nullptr, &cmd_pool));
    }


    BadaSwapchain swapchain;
    if (!bada__init_swapchain(&swapchain, physical_device, surface, device, VK_NULL_HANDLE))
    {
        bada_log_error("Failed to create swapchain");
        return false;
    }
    // Prepare rendering resources
    BadaRenderingResource rendering_resources[2] = {};
    static_assert(sizeof(rendering_resources) == sizeof(bada->rendering_resources));
    {
        for (auto& rr : rendering_resources)
        {
            // Semaphores
            VkSemaphoreCreateInfo semaphore_ci = { VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO };
            bada_vk_check(vkCreateSemaphore(device, &semaphore_ci, nullptr, &rr.image_available_semaphore));
            bada_vk_check(vkCreateSemaphore(device, &semaphore_ci, nullptr, &rr.rendering_finished_semaphore));
            // Fence
            VkFenceCreateInfo fence_ci = { VK_STRUCTURE_TYPE_FENCE_CREATE_INFO };
            fence_ci.flags = VK_FENCE_CREATE_SIGNALED_BIT;
            bada_vk_check(vkCreateFence(device, &fence_ci, nullptr, &rr.fence));
            // Command buffer
            VkCommandBufferAllocateInfo cmd_buffer_ai = { VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO };
            cmd_buffer_ai.commandPool = cmd_pool;
            cmd_buffer_ai.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
            cmd_buffer_ai.commandBufferCount = 1;
            bada_vk_check(vkAllocateCommandBuffers(device, &cmd_buffer_ai, &rr.cmd_buffer));
        }
    }


    /*
    BadaVertex vertices[] = {
        {-0.7f, -0.7f, 0.f, 1.f,    1.f, 0.f, 0.f, 0.f,     -0.1f, -0.1f},
        {-0.7f, 0.7f, 0.f, 1.f,     0.f, 1.f, 0.f, 0.f,     -0.1f, 1.1f},
        {0.7f, -0.7f, 0.f, 1.f,     0.f, 0.f, 1.f, 0.f,     1.1f, -0.1f},
        {0.7f, 0.7f, 0.f, 1.f,      0.3f, 0.3f, 0.3f, 0.f,  1.1f, 1.1f},

        {-0.2f, -0.2f, -0.5f, 1.f,    1.f, 0.f, 0.f, 0.f,     -0.1f, -0.1f},
        {-0.2f, 1.2f, -0.5f, 1.f,     0.f, 1.f, 0.f, 0.f,     -0.1f, 1.1f},
        {1.2f, -0.2f, -0.5f, 1.f,     0.f, 0.f, 1.f, 0.f,     1.1f, -0.1f},
        {1.2f, 1.2f, -0.5f, 1.f,      0.3f, 0.3f, 0.3f, 0.f,  1.1f, 1.1f},
    };

    auto render_pass = bada__create_render_pass(device, &swapchain);
    if (render_pass == VK_NULL_HANDLE)
    {
        bada_log_error("Failed to create render pass");
        return false;
    }

    // Create Vertex Buffer
    BadaBuffer vertex_buffer = {};
    if (!bada_init_buffer(
        &vertex_buffer,
        physical_device, device,
        sizeof(vertices),
        VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT))
    {
        bada_log_error("Failed to create vulkan buffer");
        return false;
    }


    // Create Staging Buffer
    BadaBuffer staging_buffer = {};
    {
        if (!bada_init_buffer(
            &staging_buffer,
            physical_device, device,
            4000000,
            VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT))
        {
            bada_log_error("Failed to create vulkan buffer");
            return false;
        }
    }
    void* staging_buffer_memptr;
    bada_vk_check(vkMapMemory(
        device, staging_buffer.memory,
        0, staging_buffer.size, 0, &staging_buffer_memptr));

    auto stage_data = [&staging_buffer, staging_buffer_memptr, physical_device, device](void* data, bada_u32 size)
    {
        bada_stage_buffer_data(&staging_buffer, staging_buffer_memptr, physical_device, device, data, size);
    };

    // Copy vertex buffer data
    {
        stage_data(vertices, vertex_buffer.size);

        VkCommandBufferBeginInfo command_buffer_bi = { VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO };
        command_buffer_bi.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
        VkCommandBuffer command_buffer = rendering_resources[0].cmd_buffer;
        {
            vkBeginCommandBuffer(command_buffer, &command_buffer_bi);
            VkBufferCopy buffer_copy_info = {};
            buffer_copy_info.size = vertex_buffer.size;
            vkCmdCopyBuffer(command_buffer, staging_buffer.handle, vertex_buffer.handle, 1, &buffer_copy_info);
            VkBufferMemoryBarrier buffer_memory_barrier = { VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER };
            buffer_memory_barrier.srcAccessMask = VK_ACCESS_MEMORY_WRITE_BIT;
            buffer_memory_barrier.dstAccessMask = VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT;
            buffer_memory_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            buffer_memory_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            buffer_memory_barrier.buffer = vertex_buffer.handle;
            buffer_memory_barrier.size = VK_WHOLE_SIZE;
            vkCmdPipelineBarrier(
                command_buffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_VERTEX_INPUT_BIT,
                0, 0, nullptr, 1, &buffer_memory_barrier, 0, nullptr);
            vkEndCommandBuffer(command_buffer);
        }

        VkSubmitInfo submit_info = { VK_STRUCTURE_TYPE_SUBMIT_INFO };
        submit_info.commandBufferCount = 1;
        submit_info.pCommandBuffers = &command_buffer;
        bada_vk_check(vkQueueSubmit(queue, 1, &submit_info, VK_NULL_HANDLE));

        vkDeviceWaitIdle(device);
    }

    // Create Image
    BadaImage image = {};
    {
        int width, height, comp;
        auto pixels = stbi_load("test.png", &width, &height, &comp, 4);
        bada_scope_exit{
            stbi_image_free(pixels);
        };
        bada_assert(pixels != nullptr);

        VkImageCreateInfo image_ci = { VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO };
        image_ci.imageType = VK_IMAGE_TYPE_2D;
        image_ci.format = VK_FORMAT_R8G8B8A8_UNORM;
        image_ci.extent = { static_cast<bada_u32>(width), static_cast<bada_u32>(height), 1 };
        image_ci.mipLevels = 1;
        image_ci.arrayLayers = 1;
        image_ci.samples = VK_SAMPLE_COUNT_1_BIT;
        image_ci.tiling = VK_IMAGE_TILING_OPTIMAL;
        image_ci.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
        image_ci.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        image_ci.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

        bada_vk_check(vkCreateImage(device, &image_ci, nullptr, &image.handle));

        VkMemoryRequirements image_memory_requirements;
        vkGetImageMemoryRequirements(device, image.handle, &image_memory_requirements);
        image.memory = bada__allocate_device_memory(
            physical_device, device, image_memory_requirements, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
        if (image.memory == VK_NULL_HANDLE)
        {
            bada_log_error("Failed to allocate vulkan memory for image");
            return false;
        }
        bada_vk_check(vkBindImageMemory(device, image.handle, image.memory, 0));

        bada_u32 image_data_size = width * height * 4;
        stage_data(pixels, image_data_size);

        VkCommandBufferBeginInfo command_buffer_bi = { VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO };
        command_buffer_bi.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
        VkCommandBuffer command_buffer = rendering_resources[0].cmd_buffer;
        vkBeginCommandBuffer(command_buffer, &command_buffer_bi);
        VkImageMemoryBarrier image_memory_barrier_from_undefined_to_transfer_dst = { VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER };
        image_memory_barrier_from_undefined_to_transfer_dst.srcAccessMask = 0;
        image_memory_barrier_from_undefined_to_transfer_dst.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        image_memory_barrier_from_undefined_to_transfer_dst.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        image_memory_barrier_from_undefined_to_transfer_dst.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
        image_memory_barrier_from_undefined_to_transfer_dst.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        image_memory_barrier_from_undefined_to_transfer_dst.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        image_memory_barrier_from_undefined_to_transfer_dst.image = image.handle;
        image_memory_barrier_from_undefined_to_transfer_dst.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        image_memory_barrier_from_undefined_to_transfer_dst.subresourceRange.baseMipLevel = 0;
        image_memory_barrier_from_undefined_to_transfer_dst.subresourceRange.levelCount = 1;
        image_memory_barrier_from_undefined_to_transfer_dst.subresourceRange.baseArrayLayer = 0;
        image_memory_barrier_from_undefined_to_transfer_dst.subresourceRange.layerCount = 1;
        vkCmdPipelineBarrier(command_buffer,
            VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT,
            0, 0, nullptr, 0, nullptr, 1, &image_memory_barrier_from_undefined_to_transfer_dst);
        VkBufferImageCopy buffer_image_copy_info = {};
        buffer_image_copy_info.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        buffer_image_copy_info.imageSubresource.mipLevel = 0;
        buffer_image_copy_info.imageSubresource.baseArrayLayer = 0;
        buffer_image_copy_info.imageSubresource.layerCount = 1;
        buffer_image_copy_info.imageOffset = { 0, 0, 0 };
        buffer_image_copy_info.imageExtent = { static_cast<bada_u32>(width), static_cast<bada_u32>(height), 1 };
        vkCmdCopyBufferToImage(command_buffer,
            staging_buffer.handle, image.handle,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &buffer_image_copy_info);
        VkImageMemoryBarrier image_memory_barrier_from_transfer_to_shader_read = { VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER };
        image_memory_barrier_from_transfer_to_shader_read.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        image_memory_barrier_from_transfer_to_shader_read.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
        image_memory_barrier_from_transfer_to_shader_read.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
        image_memory_barrier_from_transfer_to_shader_read.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        image_memory_barrier_from_transfer_to_shader_read.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        image_memory_barrier_from_transfer_to_shader_read.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        image_memory_barrier_from_transfer_to_shader_read.image = image.handle;
        image_memory_barrier_from_transfer_to_shader_read.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        image_memory_barrier_from_transfer_to_shader_read.subresourceRange.baseMipLevel = 0;
        image_memory_barrier_from_transfer_to_shader_read.subresourceRange.levelCount = 1;
        image_memory_barrier_from_transfer_to_shader_read.subresourceRange.baseArrayLayer = 0;
        image_memory_barrier_from_transfer_to_shader_read.subresourceRange.layerCount = 1;
        vkCmdPipelineBarrier(command_buffer,
            VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
            0, 0, nullptr, 0, nullptr, 1, &image_memory_barrier_from_transfer_to_shader_read);
        vkEndCommandBuffer(command_buffer);

        VkSubmitInfo submit_info = { VK_STRUCTURE_TYPE_SUBMIT_INFO };
        submit_info.commandBufferCount = 1;
        submit_info.pCommandBuffers = &command_buffer;
        bada_vk_check(vkQueueSubmit(queue, 1, &submit_info, VK_NULL_HANDLE));

        vkDeviceWaitIdle(device);

        image.view = bada__create_image_view(
            device, image.handle, image_ci.format, 0, image_ci.mipLevels, 0, image_ci.arrayLayers);
        if (image.view == VK_NULL_HANDLE)
        {
            bada_log_error("Failed to craete image view");
            return false;
        }
        image.width = width;
        image.height = height;

        VkSamplerCreateInfo sampler_ci = { VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO };
        sampler_ci.magFilter = VK_FILTER_LINEAR;
        sampler_ci.minFilter = VK_FILTER_LINEAR;
        sampler_ci.mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST;
        sampler_ci.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
        sampler_ci.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
        sampler_ci.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
        sampler_ci.maxAnisotropy = 1.f;
        sampler_ci.compareOp = VK_COMPARE_OP_ALWAYS;
        sampler_ci.borderColor = VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK;

        bada_vk_check(vkCreateSampler(device, &sampler_ci, nullptr, &image.sampler));
    }


    BadaBuffer uniform_buffer;
    {
        bada_init_buffer(&uniform_buffer, physical_device, device, 16 * sizeof(float),
            VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

        bada__test_copy_uniform_buffer(
            &uniform_buffer,
            physical_device, device,
            &swapchain,
            &staging_buffer, staging_buffer_memptr,
            rendering_resources[0].cmd_buffer,
            queue);
    }


    // Create descriptor pool
    VkDescriptorPool desc_pool;
    VkDescriptorPoolSize pool_sizes[2] = {};
    {
        pool_sizes[0].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        pool_sizes[0].descriptorCount = 1;

        pool_sizes[1].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        pool_sizes[1].descriptorCount = 1;

        VkDescriptorPoolCreateInfo create_info = { VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO };
        create_info.maxSets = 1;
        create_info.poolSizeCount = bada_array_count(pool_sizes);
        create_info.pPoolSizes = pool_sizes;
        bada_vk_check(vkCreateDescriptorPool(device, &create_info, nullptr, &desc_pool));
    }

    // Create descriptor set
    BadaDescriptorSet desc_set = {};
    {
        VkDescriptorSetLayout layout;
        {
            VkDescriptorSetLayoutBinding bindings[2] = {};

            bindings[0].binding = 0;
            bindings[0].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
            bindings[0].descriptorCount = 1;
            bindings[0].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

            bindings[1].binding = 1;
            bindings[1].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
            bindings[1].descriptorCount = 1;
            bindings[1].stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

            VkDescriptorSetLayoutCreateInfo create_info = { VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO };
            create_info.bindingCount = bada_array_count(bindings);
            create_info.pBindings = bindings;

            bada_vk_check(vkCreateDescriptorSetLayout(device, &create_info, nullptr, &layout));
        }

        VkDescriptorSet handle;
        {
            VkDescriptorSetAllocateInfo allocate_info = { VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO };
            allocate_info.descriptorPool = desc_pool;
            allocate_info.descriptorSetCount = 1;
            allocate_info.pSetLayouts = &layout;

            bada_vk_check(vkAllocateDescriptorSets(device, &allocate_info, &handle));

            VkDescriptorImageInfo image_info = {};
            image_info.sampler = image.sampler;
            image_info.imageView = image.view;
            image_info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

            VkDescriptorBufferInfo buffer_info = {};
            buffer_info.buffer = uniform_buffer.handle;
            buffer_info.range = uniform_buffer.size;

            VkWriteDescriptorSet writes[] =
            {
                { VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET },
                { VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET },
            };

            writes[0].dstSet = handle;
            writes[0].dstBinding = 0;
            writes[0].descriptorCount = 1;
            writes[0].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
            writes[0].pImageInfo = &image_info;

            writes[1].dstSet = handle;
            writes[1].dstBinding = 1;
            writes[1].descriptorCount = 1;
            writes[1].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
            writes[1].pBufferInfo = &buffer_info;

            vkUpdateDescriptorSets(device, bada_array_count(writes), writes, 0, nullptr);
        }

        desc_set.handle = handle;
        desc_set.layout = layout;
    }

    // TODO(illkwon): Change order of members (pipeline)
    BadaPipeline pipeline;
    if (!bada__init_pipeline(&pipeline, device, render_pass, desc_set.layout))
    {
        bada_log_error("Failed to create pipeline");
        return false;
    }*/


    bada->library = vulkan_library;
    bada->instance = instance;
#if bada_debug
    bada->debug_report_callback = debug_report_callback;
#endif
    bada->surface = surface;
    bada->physical_device = physical_device;
    bada->queue_family_index = queue_family_index;
    bada->device = device;
    bada->queue = queue;
    bada->cmd_pool = cmd_pool;
    bada->swapchain = swapchain;
    bada_array_copy(rendering_resources, bada->rendering_resources);
    bada->next_resource_index = 0;
    /*bada->render_pass = render_pass;
    bada->pipeline = pipeline;
    bada->vertex_buffer = vertex_buffer;
    bada->staging_buffer = staging_buffer;
    bada->staging_buffer_memptr = staging_buffer_memptr;
    bada->test_image = image;
    bada->test_desc_pool = desc_pool;
    bada->test_desc_set = desc_set;
    bada->test_uniform_buffer = uniform_buffer;*/

    return true;
}

void bada_deinit(BadaContext* bada)
{
    if (bada->device != VK_NULL_HANDLE)
    {
        vkDeviceWaitIdle(bada->device);

        /*vkDestroyDescriptorSetLayout(bada->device, bada->test_desc_set.layout, nullptr);
        bada->test_desc_set = {};

        vkDestroyDescriptorPool(bada->device, bada->test_desc_pool, nullptr);
        bada->test_desc_pool = VK_NULL_HANDLE;

        vkDestroySampler(bada->device, bada->test_image.sampler, nullptr);
        vkDestroyImageView(bada->device, bada->test_image.view, nullptr);
        vkFreeMemory(bada->device, bada->test_image.memory, nullptr);
        vkDestroyImage(bada->device, bada->test_image.handle, nullptr);
        bada->test_image = {};

        vkUnmapMemory(bada->device, bada->staging_buffer.memory);
        bada->staging_buffer_memptr = nullptr;
        bada_deinit_buffer(&bada->staging_buffer, bada->device);
        bada_deinit_buffer(&bada->test_uniform_buffer, bada->device);
        bada_deinit_buffer(&bada->vertex_buffer, bada->device);*/

        for (auto& rr : bada->rendering_resources)
        {
            if (rr.cmd_buffer != VK_NULL_HANDLE)
            {
                vkFreeCommandBuffers(bada->device, bada->cmd_pool, 1, &rr.cmd_buffer);
                rr.cmd_buffer = VK_NULL_HANDLE;
            }
            if (rr.framebuffer.handle != VK_NULL_HANDLE)
            {
                vkDestroyFramebuffer(bada->device, rr.framebuffer.handle, nullptr);
                rr.framebuffer.handle = VK_NULL_HANDLE;
            }
            if (rr.framebuffer.image_view != VK_NULL_HANDLE)
            {
                vkDestroyImageView(bada->device, rr.framebuffer.image_view, nullptr);
                rr.framebuffer.image_view = VK_NULL_HANDLE;
            }
            if (rr.fence != VK_NULL_HANDLE)
            {
                vkDestroyFence(bada->device, rr.fence, nullptr);
            }
            if (rr.rendering_finished_semaphore != VK_NULL_HANDLE)
            {
                vkDestroySemaphore(bada->device, rr.rendering_finished_semaphore, nullptr);
                rr.rendering_finished_semaphore = VK_NULL_HANDLE;
            }

            if (rr.image_available_semaphore != VK_NULL_HANDLE)
            {
                vkDestroySemaphore(bada->device, rr.image_available_semaphore, nullptr);
                rr.image_available_semaphore = VK_NULL_HANDLE;
            }
        }

        /*bada_deinit_pipeline(&bada->pipeline, bada->device);

        if (bada->render_pass != VK_NULL_HANDLE)
        {
            vkDestroyRenderPass(bada->device, bada->render_pass, nullptr);
            bada->render_pass = VK_NULL_HANDLE;
        }*/

        if (bada->swapchain.handle != VK_NULL_HANDLE)
        {
            vkDestroySwapchainKHR(bada->device, bada->swapchain.handle, nullptr);
            bada->swapchain = {};
        }

        if (bada->cmd_pool != VK_NULL_HANDLE)
        {
            vkDestroyCommandPool(bada->device, bada->cmd_pool, nullptr);
            bada->cmd_pool = VK_NULL_HANDLE;
        }

        vkDestroyDevice(bada->device, nullptr);
        bada->device = VK_NULL_HANDLE;
    }

    if (bada->surface != VK_NULL_HANDLE)
    {
        vkDestroySurfaceKHR(bada->instance, bada->surface, nullptr);
        bada->surface = VK_NULL_HANDLE;
    }

#if bada_debug
    if (bada->debug_report_callback != VK_NULL_HANDLE)
    {
        vkDestroyDebugReportCallbackEXT(bada->instance, bada->debug_report_callback, nullptr);
        bada->debug_report_callback = VK_NULL_HANDLE;
    }
#endif

    if (bada->instance != VK_NULL_HANDLE)
    {
        vkDestroyInstance(bada->instance, nullptr);
        bada->instance = VK_NULL_HANDLE;
    }

    if (bada->library)
    {
#ifdef _WIN32
        FreeLibrary((HMODULE)bada->library);
#endif
        bada->library = nullptr;
    }
}

bool bada_on_window_size_changed(BadaContext* bada)
{
    bada_log_trace("bada_on_window_size_changed");

    vkDeviceWaitIdle(bada->device);

    /*bada_deinit_pipeline(&bada->pipeline, bada->device);

    if (bada->render_pass != VK_NULL_HANDLE)
    {
        vkDestroyRenderPass(bada->device, bada->render_pass, nullptr);
        bada->render_pass = VK_NULL_HANDLE;
    }*/


    auto old_swapchain = bada->swapchain.handle;
    BadaSwapchain swapchain;
    if (!bada__init_swapchain(&swapchain, bada->physical_device, bada->surface, bada->device, old_swapchain))
    {
        bada_log_error("Failed to create swapchain");
        return false;
    }
    /*auto render_pass = bada__create_render_pass(bada->device, &swapchain);
    if (render_pass == VK_NULL_HANDLE)
    {
        bada_log_error("Failed to create render pass");
        return false;
    }
    
    BadaPipeline pipeline;
    if (!bada__init_pipeline(&pipeline, bada->device, render_pass, bada->test_desc_set.layout))
    {
        bada_log_error("Failed to create pipeline");
        return false;
    }*/

    vkDestroySwapchainKHR(bada->device, old_swapchain, nullptr);
    bada->swapchain = swapchain;
    /*bada->render_pass = render_pass;
    bada->pipeline = pipeline;

    bada__test_copy_uniform_buffer(
        &bada->test_uniform_buffer,
        bada->physical_device, bada->device,
        &bada->swapchain,
        &bada->staging_buffer, bada->staging_buffer_memptr,
        bada->rendering_resources[0].cmd_buffer,
        bada->queue);*/


    return true;
}



//
// Internal
//

bool bada_render_begin(BadaContext* bada, BadaRenderContext* render_context, VkRenderPass render_pass)
{
    render_context->rendering_resource = &bada->rendering_resources[bada->next_resource_index];

    bada->next_resource_index = (bada->next_resource_index + 1) % bada_array_count(bada->rendering_resources);
    bada_vk_check(vkWaitForFences(bada->device, 1, &render_context->rendering_resource->fence, VK_FALSE, 100000000));
    vkResetFences(bada->device, 1, &render_context->rendering_resource->fence);

    VkResult result = vkAcquireNextImageKHR(
        bada->device, bada->swapchain.handle, UINT64_MAX,
        render_context->rendering_resource->image_available_semaphore, VK_NULL_HANDLE,
        &render_context->image_index);
    switch (result)
    {
    case VK_SUCCESS:
    case VK_SUBOPTIMAL_KHR:
        break;
    case VK_ERROR_OUT_OF_DATE_KHR:
        bada_log_trace("VK_ERROR_OUT_OF_DATE_KHR -> bada_on_window_size_changed");
        return bada_on_window_size_changed(bada);
    default:
        bada_log_error("Failed to acquire image");
        return false;
    }

    // Create new framebuffer every frame
    {
        if (render_context->rendering_resource->framebuffer.handle != VK_NULL_HANDLE)
        {
            vkDestroyFramebuffer(bada->device, render_context->rendering_resource->framebuffer.handle, nullptr);
            render_context->rendering_resource->framebuffer.handle = VK_NULL_HANDLE;
        }
        if (render_context->rendering_resource->framebuffer.image_view != VK_NULL_HANDLE)
        {
            vkDestroyImageView(bada->device, render_context->rendering_resource->framebuffer.image_view, nullptr);
            render_context->rendering_resource->framebuffer.image_view = VK_NULL_HANDLE;
        }

        BadaFramebuffer framebuffer;

        framebuffer.image_view = bada__create_image_view(
            bada->device, bada->swapchain.images[render_context->image_index], bada->swapchain.format, 0, 1, 0, 1);
        if (framebuffer.image_view == VK_NULL_HANDLE)
        {
            bada_log_error("Failed to craete image view");
            return false;
        }

        VkFramebufferCreateInfo framebuffer_ci = { VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO };
        framebuffer_ci.renderPass = render_pass;
        framebuffer_ci.attachmentCount = 1;
        framebuffer_ci.pAttachments = &framebuffer.image_view;
        framebuffer_ci.width = bada->swapchain.extent.width;
        framebuffer_ci.height = bada->swapchain.extent.height;
        framebuffer_ci.layers = 1;

        bada_vk_check(vkCreateFramebuffer(bada->device, &framebuffer_ci, nullptr, &framebuffer.handle));

        render_context->rendering_resource->framebuffer = framebuffer;
    }

    return true;
}

bool bada_render_end(BadaContext* bada, BadaRenderContext* render_context)
{
    VkPipelineStageFlags wait_dst_stage_mask = VK_PIPELINE_STAGE_TRANSFER_BIT;
    VkSubmitInfo submit_info = { VK_STRUCTURE_TYPE_SUBMIT_INFO };
    submit_info.waitSemaphoreCount = 1;
    submit_info.pWaitSemaphores = &render_context->rendering_resource->image_available_semaphore;
    submit_info.pWaitDstStageMask = &wait_dst_stage_mask;
    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &render_context->rendering_resource->cmd_buffer;
    submit_info.signalSemaphoreCount = 1;
    submit_info.pSignalSemaphores = &render_context->rendering_resource->rendering_finished_semaphore;
    bada_vk_check(vkQueueSubmit(bada->queue, 1, &submit_info, render_context->rendering_resource->fence));

    VkPresentInfoKHR present_info = { VK_STRUCTURE_TYPE_PRESENT_INFO_KHR };
    present_info.waitSemaphoreCount = 1;
    present_info.pWaitSemaphores = &render_context->rendering_resource->rendering_finished_semaphore;
    present_info.swapchainCount = 1;
    present_info.pSwapchains = &bada->swapchain.handle;
    present_info.pImageIndices = &render_context->image_index;
    VkResult result = vkQueuePresentKHR(bada->queue, &present_info);
    switch (result)
    {
    case VK_SUCCESS:
        break;
    case VK_ERROR_OUT_OF_DATE_KHR:
    case VK_SUBOPTIMAL_KHR:
        bada_log_trace("VK_SUBOPTIMAL_KHR -> bada_on_window_size_changed");
        return bada_on_window_size_changed(bada);
    default:
        bada_log_error("Failed to present image");
        return false;
    }

    *render_context = {};
    return true;
}

[[deprecated]]
bool bada_render(BadaContext* bada)
{
    auto current_rendering_resource = &bada->rendering_resources[bada->next_resource_index];

    bada->next_resource_index = (bada->next_resource_index + 1) % bada_array_count(bada->rendering_resources);
    bada_vk_check(vkWaitForFences(bada->device, 1, &current_rendering_resource->fence, VK_FALSE, 100000000));
    vkResetFences(bada->device, 1, &current_rendering_resource->fence);

    bada_u32 image_index;
    //VkResult result = vkAcquireNextImageKHR(bada->device, bada->swapchain.handle, UINT64_MAX, bada->image_available_semaphore, VK_NULL_HANDLE, &image_index);
    VkResult result = vkAcquireNextImageKHR(
        bada->device, bada->swapchain.handle, UINT64_MAX, current_rendering_resource->image_available_semaphore, VK_NULL_HANDLE, &image_index);
    switch (result)
    {
    case VK_SUCCESS:
    case VK_SUBOPTIMAL_KHR:
        break;
    case VK_ERROR_OUT_OF_DATE_KHR:
        bada_log_trace("VK_ERROR_OUT_OF_DATE_KHR -> bada_on_window_size_changed");
        return bada_on_window_size_changed(bada);
    default:
        bada_log_error("Failed to acquire image");
        return false;
    }

    // Create new framebuffer every frame
    /*if (current_rendering_resource->framebuffer.handle != VK_NULL_HANDLE)
    {
        vkDestroyFramebuffer(bada->device, current_rendering_resource->framebuffer.handle, nullptr);
        current_rendering_resource->framebuffer.handle = VK_NULL_HANDLE;
    }
    if (current_rendering_resource->framebuffer.image_view != VK_NULL_HANDLE)
    {
        vkDestroyImageView(bada->device, current_rendering_resource->framebuffer.image_view, nullptr);
        current_rendering_resource->framebuffer.image_view = VK_NULL_HANDLE;
    }
    {
        BadaFramebuffer framebuffer;

        framebuffer.image_view = bada__create_image_view(
            bada->device, bada->swapchain.images[image_index], bada->swapchain.format, 0, 1, 0, 1);
        if (framebuffer.image_view == VK_NULL_HANDLE)
        {
            bada_log_error("Failed to craete image view");
            return false;
        }

        VkFramebufferCreateInfo framebuffer_ci = { VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO };
        //framebuffer_ci.renderPass = bada->render_pass;
        framebuffer_ci.attachmentCount = 1;
        framebuffer_ci.pAttachments = &framebuffer.image_view;
        framebuffer_ci.width = bada->swapchain.extent.width;
        framebuffer_ci.height = bada->swapchain.extent.height;
        framebuffer_ci.layers = 1;

        bada_vk_check(vkCreateFramebuffer(bada->device, &framebuffer_ci, nullptr, &framebuffer.handle));

        current_rendering_resource->framebuffer = framebuffer;
    }*/


    // Record command buffers
    {
        /*VkRenderPassBeginInfo render_pass_bi = { VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO };
        render_pass_bi.renderPass = bada->render_pass;
        render_pass_bi.framebuffer = current_rendering_resource->framebuffer.handle;
        render_pass_bi.renderArea.offset = { 0, 0 };
        render_pass_bi.renderArea.extent = bada->swapchain.extent;
        render_pass_bi.clearValueCount = 1;
        VkClearValue clear_value = { 1.f, 0.8f, 0.4f, 0.f };
        render_pass_bi.pClearValues = &clear_value;*/

        auto cmd_buffer = current_rendering_resource->cmd_buffer;

        VkCommandBufferBeginInfo cmd_buffer_bi = { VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO };
        cmd_buffer_bi.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

        vkBeginCommandBuffer(cmd_buffer, &cmd_buffer_bi);
        VkImageMemoryBarrier image_memory_barrier = { VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER };
        image_memory_barrier.srcAccessMask = 0;
        image_memory_barrier.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;;
        image_memory_barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        image_memory_barrier.newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
        image_memory_barrier.srcQueueFamilyIndex = bada->queue_family_index;
        image_memory_barrier.dstQueueFamilyIndex = bada->queue_family_index;
        image_memory_barrier.image = bada->swapchain.images[image_index];
        image_memory_barrier.subresourceRange = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 };
        vkCmdPipelineBarrier(cmd_buffer, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
            0, 0, nullptr, 0, nullptr, 1, &image_memory_barrier);
        /*vkCmdBeginRenderPass(cmd_buffer, &render_pass_bi, VK_SUBPASS_CONTENTS_INLINE);
        vkCmdBindPipeline(cmd_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, bada->pipeline.handle);
        vkCmdBindDescriptorSets(cmd_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, bada->pipeline.layout, 0, 1, &bada->test_desc_set.handle, 0, nullptr);
        VkViewport viewport = {};
        viewport.width = static_cast<float>(bada->swapchain.extent.width);
        viewport.height = static_cast<float>(bada->swapchain.extent.height);
        viewport.minDepth = 0.f;
        viewport.maxDepth = 1.f;
        VkRect2D scissor = {};
        scissor.extent = bada->swapchain.extent;
        vkCmdSetViewport(cmd_buffer, 0, 1, &viewport);
        vkCmdSetScissor(cmd_buffer, 0, 1, &scissor);
        VkDeviceSize offset = 0;
        vkCmdBindVertexBuffers(cmd_buffer, 0, 1, &bada->vertex_buffer.handle, &offset);
        vkCmdDraw(cmd_buffer, 8, 1, 0, 0);
        vkCmdEndRenderPass(cmd_buffer);*/
        bada_vk_check(vkEndCommandBuffer(cmd_buffer));
    }


    VkPipelineStageFlags wait_dst_stage_mask = VK_PIPELINE_STAGE_TRANSFER_BIT;
    VkSubmitInfo submit_info = { VK_STRUCTURE_TYPE_SUBMIT_INFO };
    submit_info.waitSemaphoreCount = 1;
    submit_info.pWaitSemaphores = &current_rendering_resource->image_available_semaphore;
    submit_info.pWaitDstStageMask = &wait_dst_stage_mask;
    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &current_rendering_resource->cmd_buffer;
    submit_info.signalSemaphoreCount = 1;
    submit_info.pSignalSemaphores = &current_rendering_resource->rendering_finished_semaphore;
    bada_vk_check(vkQueueSubmit(bada->queue, 1, &submit_info, current_rendering_resource->fence));

    VkPresentInfoKHR present_info = { VK_STRUCTURE_TYPE_PRESENT_INFO_KHR };
    present_info.waitSemaphoreCount = 1;
    present_info.pWaitSemaphores = &current_rendering_resource->rendering_finished_semaphore;
    present_info.swapchainCount = 1;
    present_info.pSwapchains = &bada->swapchain.handle;
    present_info.pImageIndices = &image_index;
    result = vkQueuePresentKHR(bada->queue, &present_info);
    switch (result)
    {
    case VK_SUCCESS:
        break;
    case VK_ERROR_OUT_OF_DATE_KHR:
    case VK_SUBOPTIMAL_KHR:
        bada_log_trace("VK_SUBOPTIMAL_KHR -> bada_on_window_size_changed");
        return bada_on_window_size_changed(bada);
    default:
        bada_log_error("Failed to present image");
        return false;
    }

    return true;
}

bool bada__is_available_vk_extension(const char* required_ext_name, VkExtensionProperties* available_exts, bada_u32 available_ext_count)
{
    // Check extension availability
    for (bada_u32 i = 0; i < available_ext_count; ++i)
    {
        auto& available_ext = available_exts[i];
        if (strcmp(required_ext_name, available_ext.extensionName) == 0)
        {
            return true;
        }
    }
    return false;
}

bool bada__init_swapchain(
    BadaSwapchain* swapchain,
    VkPhysicalDevice physical_device,
    VkSurfaceKHR surface,
    VkDevice device,
    VkSwapchainKHR old)
{
    VkSurfaceCapabilitiesKHR surface_cap;
    bada_vk_check(vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physical_device, surface, &surface_cap));

    const bada_u32 image_count = bada_swapchain_image_count;
    if (bada_swapchain_image_count < surface_cap.minImageCount)
    {
        bada_log_error("Capable min image count is greater than bada_swapchain_image_count");
        return false;
        //image_count = surface_cap.minImageCount;
    }
    if (surface_cap.maxImageCount > 0 &&
        image_count > surface_cap.maxImageCount)
    {
        bada_log_error("Capable max image count is less than bada_swapchain_image_count");
        return false;
        //image_count = surface_cap.maxImageCount;
    }

    VkExtent2D swapchain_extent;
    if (surface_cap.currentExtent.width == -1)
    {
        swapchain_extent = { 640, 480 };
        if (swapchain_extent.width < surface_cap.minImageExtent.width)
        {
            swapchain_extent.width = surface_cap.minImageExtent.width;
        }
        if (swapchain_extent.width > surface_cap.maxImageExtent.width)
        {
            swapchain_extent.width = surface_cap.maxImageExtent.width;
        }
        if (swapchain_extent.height < surface_cap.minImageExtent.height)
        {
            swapchain_extent.height = surface_cap.minImageExtent.height;
        }
        if (swapchain_extent.height > surface_cap.maxImageExtent.height)
        {
            swapchain_extent.height = surface_cap.maxImageExtent.height;
        }
    }
    else
    {
        swapchain_extent = surface_cap.currentExtent;
    }

    VkImageUsageFlags usage_flags;
    if (surface_cap.supportedUsageFlags & VK_IMAGE_USAGE_TRANSFER_DST_BIT)
    {
        usage_flags = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT |
            VK_IMAGE_USAGE_TRANSFER_DST_BIT;
    }
    else
    {
        bada_log_error("Surface doesn't support image usage");
        return false;
    }

    VkSurfaceTransformFlagBitsKHR pre_transform;
    if (surface_cap.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR)
    {
        pre_transform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
    }
    else
    {
        pre_transform = surface_cap.currentTransform;
    }

    // Choose surface format
    VkSurfaceFormatKHR selected_surface_format;
    {
        bada_u32 count;
        bada_vk_check(vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device, surface, &count, nullptr));
        bada_vla(surface_formats, VkSurfaceFormatKHR, count);
        bada_vk_check(vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device, surface, &surface_formats.count, surface_formats.data));

        // There is no preferred format
        if (surface_formats.count == 1 && surface_formats.data[0].format == VK_FORMAT_UNDEFINED)
        {
            selected_surface_format = { VK_FORMAT_R8G8B8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
            goto SURFACE_FORMAT_SELECTED;
        }
        for (bada_u32 i = 0; i < surface_formats.count; ++i)
        {
            auto surface_format = surface_formats.data[i];
            if (surface_format.format == VK_FORMAT_R8G8B8A8_UNORM)
            {
                selected_surface_format = surface_format;
                goto SURFACE_FORMAT_SELECTED;
            }
        }
        selected_surface_format = surface_formats.data[0];
    }
    SURFACE_FORMAT_SELECTED:

    // Select present mode
    // TODO(illkwon): Try mailbox!
    VkPresentModeKHR selected_present_mode;
    {
        bada_u32 count;
        bada_vk_check(vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device, surface, &count, nullptr))
        bada_vla(present_modes, VkPresentModeKHR, count);
        bada_vk_check(vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device, surface, &present_modes.count, present_modes.data));
        for (bada_u32 i = 0; i < present_modes.count; ++i)
        {
            auto present_mode = present_modes.data[i];
            if (present_mode == VK_PRESENT_MODE_FIFO_KHR)
            {
                selected_present_mode = present_mode;
                goto PRESENT_MODE_SELECTED;
            }
        }
        bada_log_error("Present mode is not selected");
        return false;
    }
    PRESENT_MODE_SELECTED:

    VkSwapchainCreateInfoKHR swapchain_ci = { VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR };
    swapchain_ci.surface = surface;
    swapchain_ci.minImageCount = image_count;
    swapchain_ci.imageFormat = selected_surface_format.format;
    swapchain_ci.imageColorSpace = selected_surface_format.colorSpace;
    swapchain_ci.imageExtent = swapchain_extent;
    swapchain_ci.imageArrayLayers = 1;
    swapchain_ci.imageUsage = usage_flags;
    swapchain_ci.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    swapchain_ci.preTransform = pre_transform;
    swapchain_ci.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    swapchain_ci.presentMode = selected_present_mode;
    swapchain_ci.clipped = VK_TRUE;
    swapchain_ci.oldSwapchain = old;

    // Create Swap Chain
    VkSwapchainKHR swapchain_handle;
    bada_vk_check(vkCreateSwapchainKHR(device, &swapchain_ci, nullptr, &swapchain_handle));

    VkImage images[bada_swapchain_image_count];
    {
        bada_u32 count;
        bada_vk_check(vkGetSwapchainImagesKHR(device, swapchain_handle, &count, nullptr));
        if (count != bada_swapchain_image_count)
        {
            bada_log_error("Swapchain image count mismatch");
            return false;
        }
        bada_vk_check(vkGetSwapchainImagesKHR(device, swapchain_handle, &count, images));
    }

    *swapchain = {};
    swapchain->handle = swapchain_handle;
    swapchain->extent = swapchain_extent;
    swapchain->format = swapchain_ci.imageFormat;
    bada_array_copy(images, swapchain->images);

    return true;
}

// TODO(illkwon): The only part this function refers to swapchain is its format.
// So it might be possible to not call it every time recreation, but once at init.
VkRenderPass bada__create_render_pass(VkDevice device, BadaSwapchain* swapchain)
{
    VkAttachmentDescription attachment_descriptions[1] = {};
    auto attachment_desc = &attachment_descriptions[0];
    attachment_desc->format = swapchain->format;
    attachment_desc->samples = VK_SAMPLE_COUNT_1_BIT;
    attachment_desc->loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    attachment_desc->storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    attachment_desc->stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    attachment_desc->stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    attachment_desc->initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    attachment_desc->finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

    VkAttachmentReference color_attachment_refs[1] = {};
    auto ref = &color_attachment_refs[0];
    ref->attachment = 0;
    ref->layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkSubpassDescription subpass_descriptions[1] = {};
    auto subpass_desc = &subpass_descriptions[0];
    subpass_desc->pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass_desc->colorAttachmentCount = bada_array_count(color_attachment_refs);
    subpass_desc->pColorAttachments = color_attachment_refs;

    VkSubpassDependency dependencies[2] = {};
    dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
    dependencies[0].dstSubpass = 0;
    dependencies[0].srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
    dependencies[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependencies[0].srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
    dependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    dependencies[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;
    dependencies[1].srcSubpass = 0;
    dependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;
    dependencies[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependencies[1].dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
    dependencies[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    dependencies[1].dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
    dependencies[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;


    VkRenderPassCreateInfo render_pass_ci = { VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO };
    render_pass_ci.attachmentCount = bada_array_count(attachment_descriptions);
    render_pass_ci.pAttachments = attachment_descriptions;
    render_pass_ci.subpassCount = bada_array_count(subpass_descriptions);
    render_pass_ci.pSubpasses = subpass_descriptions;
    render_pass_ci.dependencyCount = bada_array_count(dependencies);
    render_pass_ci.pDependencies = dependencies;

    VkRenderPass render_pass;
    if (vkCreateRenderPass(device, &render_pass_ci, nullptr, &render_pass) != VK_SUCCESS)
    {
        return VK_NULL_HANDLE;
    }

    return render_pass;
}

VkShaderModule bada_create_shader_module(VkDevice device, const char* filename)
{
    auto code = read_file(filename);
    if (!code.success)
    {
        return VK_NULL_HANDLE;
    }
    bada_scope_exit{ free(code.contents); };

    VkShaderModuleCreateInfo shader_module_ci = { VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO };
    shader_module_ci.codeSize = code.size;
    shader_module_ci.pCode = static_cast<bada_u32*>(code.contents);
    VkShaderModule shader_module;
    if (vkCreateShaderModule(device, &shader_module_ci, nullptr, &shader_module) != VK_SUCCESS)
    {
        return VK_NULL_HANDLE;
    }

    return shader_module;
}

bool bada__init_pipeline(BadaPipeline* pipeline, VkDevice device, VkRenderPass render_pass, VkDescriptorSetLayout desc_set_layout)
{
    auto vert = bada_create_shader_module(device, "shader.vert.spv");
    auto frag = bada_create_shader_module(device, "shader.frag.spv");
    if (!vert || !frag)
    {
        return false;
    }
    bada_scope_exit{
        vkDestroyShaderModule(device, vert, nullptr);
        vkDestroyShaderModule(device, frag, nullptr);
    };

    VkPipelineShaderStageCreateInfo shader_stage_cis[] =
    {
        {VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO},
        {VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO}
    };
    shader_stage_cis[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
    shader_stage_cis[0].module = vert;
    shader_stage_cis[0].pName = "main";
    shader_stage_cis[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
    shader_stage_cis[1].module = frag;
    shader_stage_cis[1].pName = "main";

    VkVertexInputBindingDescription vert_binding_descs[1] = {};
    vert_binding_descs[0].binding = 0;
    vert_binding_descs[0].stride = sizeof(BadaVertex);
    vert_binding_descs[0].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

    VkVertexInputAttributeDescription vert_attrib_descs[3] = {};
    vert_attrib_descs[0].location = 0;
    vert_attrib_descs[0].binding = vert_binding_descs[0].binding;
    vert_attrib_descs[0].format = VK_FORMAT_R32G32B32A32_SFLOAT;
    vert_attrib_descs[0].offset = offsetof(BadaVertex, x);
    vert_attrib_descs[1].location = 1;
    vert_attrib_descs[1].binding = vert_binding_descs[0].binding;
    vert_attrib_descs[1].format = VK_FORMAT_R32G32B32A32_SFLOAT;
    vert_attrib_descs[1].offset = offsetof(BadaVertex, r);
    vert_attrib_descs[2].location = 2;
    vert_attrib_descs[2].binding = vert_binding_descs[0].binding;
    vert_attrib_descs[2].format = VK_FORMAT_R32G32_SFLOAT;
    vert_attrib_descs[2].offset = offsetof(BadaVertex, u);

    VkPipelineVertexInputStateCreateInfo vert_input_state_ci = { VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO };
    vert_input_state_ci.vertexBindingDescriptionCount = bada_array_count(vert_binding_descs);
    vert_input_state_ci.pVertexBindingDescriptions = vert_binding_descs;
    vert_input_state_ci.vertexAttributeDescriptionCount = bada_array_count(vert_attrib_descs);
    vert_input_state_ci.pVertexAttributeDescriptions = vert_attrib_descs;

    VkPipelineInputAssemblyStateCreateInfo input_assembly_state_ci = { VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO };
    input_assembly_state_ci.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP;

    /*VkViewport viewport = {};
    viewport.x = 0.f;
    viewport.y = 0.f;
    viewport.width = 300.f;
    viewport.height = 300.f;
    viewport.minDepth = 0.f;
    viewport.maxDepth = 1.f;

    VkRect2D scissor = {};
    scissor.offset = { 0, 0 };
    scissor.extent = { 300, 300 };*/

    VkPipelineViewportStateCreateInfo viewport_state_ci = { VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO };
    viewport_state_ci.viewportCount = 1;
    //viewport_state_ci.pViewports = &viewport;
    viewport_state_ci.scissorCount = 1;
    //viewport_state_ci.pScissors = &scissor;

    VkPipelineRasterizationStateCreateInfo rasterization_state_ci = { VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO };
    rasterization_state_ci.polygonMode = VK_POLYGON_MODE_FILL;
    rasterization_state_ci.cullMode = VK_CULL_MODE_BACK_BIT;
    rasterization_state_ci.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    rasterization_state_ci.lineWidth = 1.f;

    VkPipelineMultisampleStateCreateInfo multisample_state_ci = { VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO };
    multisample_state_ci.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    multisample_state_ci.minSampleShading = 1.f;

    VkPipelineColorBlendAttachmentState color_blend_attachment_state = {};
    color_blend_attachment_state.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
    color_blend_attachment_state.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
    color_blend_attachment_state.colorBlendOp = VK_BLEND_OP_ADD;
    color_blend_attachment_state.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
    color_blend_attachment_state.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    color_blend_attachment_state.alphaBlendOp = VK_BLEND_OP_ADD;
    color_blend_attachment_state.colorWriteMask =
        VK_COLOR_COMPONENT_R_BIT |
        VK_COLOR_COMPONENT_G_BIT |
        VK_COLOR_COMPONENT_B_BIT |
        VK_COLOR_COMPONENT_A_BIT;

    VkPipelineColorBlendStateCreateInfo color_blend_state_ci = { VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO };
    color_blend_state_ci.logicOp = VK_LOGIC_OP_COPY;
    color_blend_state_ci.attachmentCount = 1;
    color_blend_state_ci.pAttachments = &color_blend_attachment_state;

    VkDynamicState dynamic_states[] = { VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR };
    VkPipelineDynamicStateCreateInfo dynamic_state_ci = { VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO };
    dynamic_state_ci.dynamicStateCount = bada_array_count(dynamic_states);
    dynamic_state_ci.pDynamicStates = dynamic_states;

    VkPipelineLayoutCreateInfo layout_ci = { VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO };
    layout_ci.setLayoutCount = 1;
    layout_ci.pSetLayouts = &desc_set_layout;
    VkPipelineLayout layout;
    bada_vk_check(vkCreatePipelineLayout(device, &layout_ci, nullptr, &layout));

    VkGraphicsPipelineCreateInfo pipeline_ci = { VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO };
    pipeline_ci.stageCount = bada_array_count(shader_stage_cis);
    pipeline_ci.pStages = shader_stage_cis;
    pipeline_ci.pVertexInputState = &vert_input_state_ci;
    pipeline_ci.pInputAssemblyState = &input_assembly_state_ci;
    pipeline_ci.pViewportState = &viewport_state_ci;
    pipeline_ci.pRasterizationState = &rasterization_state_ci;
    pipeline_ci.pMultisampleState = &multisample_state_ci;
    pipeline_ci.pColorBlendState = &color_blend_state_ci;
    pipeline_ci.pDynamicState = &dynamic_state_ci;
    pipeline_ci.layout = layout;
    pipeline_ci.renderPass = render_pass;
    pipeline_ci.basePipelineIndex = -1;

    VkPipeline handle;
    bada_vk_check(vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, 1, &pipeline_ci, nullptr, &handle));

    *pipeline = {};
    pipeline->handle = handle;
    pipeline->layout = layout;

    return true;
}

void bada_deinit_pipeline(BadaPipeline* pipeline, VkDevice device)
{
    if (pipeline->layout != VK_NULL_HANDLE)
    {
        vkDestroyPipelineLayout(device, pipeline->layout, nullptr);
    }
    if (pipeline->handle != VK_NULL_HANDLE)
    {
        vkDestroyPipeline(device, pipeline->handle, nullptr);
    }
    *pipeline = {};
}

VkImageView bada__create_image_view(
    VkDevice device,
    VkImage image,
    VkFormat view_format,
    bada_u32 base_mip_level,
    bada_u32 level_count,
    bada_u32 base_array_layer,
    bada_u32 layer_count)
{
    VkImageViewCreateInfo image_view_ci = { VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO };
    image_view_ci.image = image;
    image_view_ci.viewType = VK_IMAGE_VIEW_TYPE_2D;
    image_view_ci.format = view_format;
    image_view_ci.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
    image_view_ci.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
    image_view_ci.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
    image_view_ci.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
    image_view_ci.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    image_view_ci.subresourceRange.baseMipLevel = base_mip_level;
    image_view_ci.subresourceRange.levelCount = level_count;
    image_view_ci.subresourceRange.baseArrayLayer = base_array_layer;
    image_view_ci.subresourceRange.layerCount = layer_count;

    VkImageView image_view;
    if (vkCreateImageView(device, &image_view_ci, nullptr, &image_view) != VK_SUCCESS)
    {
        return VK_NULL_HANDLE;
    }
    
    return image_view;
}

bool bada__init_image(BadaImage* image, VkDevice device, bada_u32 width, bada_u32 height)
{
    VkImageCreateInfo image_ci = { VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO };
    image_ci.imageType = VK_IMAGE_TYPE_2D;
    image_ci.format = VK_FORMAT_R8G8B8A8_UNORM;
    image_ci.extent = { static_cast<bada_u32>(width), static_cast<bada_u32>(height), 1 };
    image_ci.mipLevels = 1;
    image_ci.arrayLayers = 1;
    image_ci.samples = VK_SAMPLE_COUNT_1_BIT;
    image_ci.tiling = VK_IMAGE_TILING_OPTIMAL;
    image_ci.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
    image_ci.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    image_ci.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

    VkImage image_handle;
    bada_vk_check(vkCreateImage(device, &image_ci, nullptr, &image_handle));

    VkImageView view = bada__create_image_view(
        device, image_handle, image_ci.format, 0, image_ci.mipLevels, 0, image_ci.arrayLayers);
    if (view == VK_NULL_HANDLE)
    {
        bada_log_error("Failed to create image");
        return false;
    }

    *image = {};
    image->width = width;
    image->height = height;
    image->handle = image_handle;
    image->view = view;

    return true;
}

bool bada_init_buffer(
    BadaBuffer* buffer,
    VkPhysicalDevice physical_device,
    VkDevice device,
    bada_u32 size,
    VkBufferUsageFlags usage,
    VkMemoryPropertyFlags memory_property)
{
    VkBufferCreateInfo buffer_ci = { VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO };
    buffer_ci.size = size;
    buffer_ci.usage = usage;
    buffer_ci.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    VkBuffer handle;
    bada_vk_check(vkCreateBuffer(device, &buffer_ci, nullptr, &handle));

    VkMemoryRequirements buffer_memory_requirements;
    vkGetBufferMemoryRequirements(device, handle, &buffer_memory_requirements);
    auto memory = bada__allocate_device_memory(
        physical_device, device, buffer_memory_requirements, memory_property);
    if (memory == VK_NULL_HANDLE)
    {
        bada_log_error("Failed to allocate memory using vulkan");
        return false;
    }

    bada_vk_check(vkBindBufferMemory(device, handle, memory, 0));

    *buffer = {};
    buffer->size = size;
    buffer->handle = handle;
    buffer->memory = memory;

    return true;
}

void bada_deinit_buffer(BadaBuffer* buffer, VkDevice device)
{
    if (buffer->handle != VK_NULL_HANDLE)
    {
        vkDestroyBuffer(device, buffer->handle, nullptr);
    }
    if (buffer->memory != VK_NULL_HANDLE)
    {
        vkFreeMemory(device, buffer->memory, nullptr);
    }
    *buffer = {};
}

void bada_stage_buffer_data(BadaBuffer* staging_buffer, void* staging_buffer_memptr, VkPhysicalDevice physical_device, VkDevice device, void* data, bada_u32 size)
{
    bada_assert(size <= staging_buffer->size);
    memcpy(staging_buffer_memptr, data, size);

    VkPhysicalDeviceProperties physical_device_props;
    vkGetPhysicalDeviceProperties(physical_device, &physical_device_props);
    VkMappedMemoryRange flush_range = { VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE };
    flush_range.memory = staging_buffer->memory;
    flush_range.offset = 0;
    if (staging_buffer->size <= physical_device_props.limits.nonCoherentAtomSize)
    {
        flush_range.size = VK_WHOLE_SIZE;
    }
    else
    {
        flush_range.size = static_cast<bada_u32>(
            size - (size % physical_device_props.limits.nonCoherentAtomSize) +
            physical_device_props.limits.nonCoherentAtomSize);
    }
    vkFlushMappedMemoryRanges(device, 1, &flush_range);
}


VkDeviceMemory bada__allocate_device_memory(
    VkPhysicalDevice physical_device,
    VkDevice device,
    VkMemoryRequirements requirements,
    VkMemoryPropertyFlags property)
{
    VkPhysicalDeviceMemoryProperties memory_properties = {};
    vkGetPhysicalDeviceMemoryProperties(physical_device, &memory_properties);
    for (bada_u32 i = 0; i < memory_properties.memoryTypeCount; ++i)
    {
        if ((requirements.memoryTypeBits & (1 << i)) &&
            (memory_properties.memoryTypes[i].propertyFlags & property))
        {
            VkMemoryAllocateInfo memory_ai = { VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO };
            memory_ai.allocationSize = requirements.size;
            memory_ai.memoryTypeIndex = i;

            VkDeviceMemory memory;
            if (vkAllocateMemory(device, &memory_ai, nullptr, &memory) != VK_SUCCESS)
            {
                return (VkDeviceMemory)VK_NULL_HANDLE;
            }

            return memory;
        }
    }

    return (VkDeviceMemory)VK_NULL_HANDLE;
}

#if bada_debug
VKAPI_ATTR
VkBool32 VKAPI_CALL bada__vk_debug_callback(
    VkDebugReportFlagsEXT flags,
    VkDebugReportObjectTypeEXT obj_type,
    bada_u64 obj,
    size_t location,
    bada_i32 code,
    const char* layer_prefix,
    const char* msg,
    void* userdata)
{
    bada_log_error("Vulkan: %s: %s", layer_prefix, msg);
    return VK_TRUE;
}
#endif

