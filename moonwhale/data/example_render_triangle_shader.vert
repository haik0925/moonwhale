#version 450

layout(set = 0, binding = 0) uniform u_uniform_buffer {
    uniform mat4 u_projection;
};

layout(location = 0) in vec3 i_position;
layout(location = 1) in vec3 i_color;

out gl_PerVertex
{
    vec4 gl_Position;
};

layout(location = 0) out vec4 v_color;

void main()
{
    gl_Position = u_projection * vec4(i_position, 1.0);
    v_color = vec4(i_color, 1.0);
}